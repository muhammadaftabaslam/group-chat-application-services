var express =           require('express')
    , http =            require('http')
    , path =            require('path')
    , morgan =          require('morgan')
    , bodyParser =      require('body-parser')
	, exports = redis_store = require('redis')
    , methodOverride =  require('method-override')
    // , cookieParser =    require('cookie-parser')
    // , cookieSession =   require('cookie-session')
    // , session =         require('express-session')
    , csrf =            require('csurf');

var app = module.exports = express();
// app.options('*', function(req, res) {
    // res.send(200);
// });
password_reset_expire_time = 600;
debugModeOn = true;
//server_url = "http://94.200.178.58:8000";
server_url = "http://dev.convolounge.com:8000";
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
process.env['RDS_HOSTNAME'] = 'cvlg-dev.ccksny0edv5o.us-west-2.rds.amazonaws.com';
process.env['RDS_USERNAME'] =  'cvlgadmin';
process.env['RDS_PASSWORD']= 'hh76ry!hhu5';
process.env['RDS_DATABASE']= 'ConvoLoungeDEV';//'ebdb';
process.env['RDS_PORT'] = '3306';



var dbConfig = require('./config/database.js');
var mysql      = require('mysql');

connection = mysql.createConnection(dbConfig.connection);
connection.connect(function(err) {            
    if(err) { 
      console.log('RDS amazon connection error :', err);
    }else
    {
        console.log("Amazon RDS mysql db connection succefull");
    }
  });    
 
var checkit  = require('checkit');
Promise = require('bluebird'); 
var knex = require('knex')(dbConfig);
bookshelf = require('bookshelf')(knex);
app.set('bookshelf', bookshelf);
app.use(allowCrossDomain);
app.set('views', __dirname + '/client/views/');
//app.set('view engine', 'jade');
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
//app.use(bodyParser());
app.use(methodOverride());
app.use(express.static(__dirname + '/server/public'));
app.set('/public', __dirname + '/server/public');
publicImagePath = app.get('/public');
//app.use(express.static(path.join(__dirname, 'client')));
//app.use(cookieParser());
//app.use(session({ secret: process.env.COOKIE_SECRET || "Superdupersecret"}));
redis = redis_store.createClient();
redis.on('error', function(err){
	console.log(("REDIS ERROR: " + redis.host + " : " + redis.port + " - " + err).red );
});

var env = process.env.NODE_ENV || 'development';
if ('development' === env || 'production' === env) {
    //app.use(csrf());
    app.use(function(req, res, next) {
    //res.cookie('XSRF-TOKEN', req.csrfToken());
        next();
    });
}
require('./server/index.js')(app);
app.use(express.static(__dirname + '/client/app/'));

	
app.use('/',function(req,res){
res.json(200,{Status:"ConvoLounge APIs"});
//res.redirect(200,{Status:"ConvoLounge APIs"});//, '/client/app/#/view1');
//res.sendfile(__dirname + '/client/app/view1');
//res.json(200,{Status:'i am ok'})
	
});
app.set('port', process.env.PORT || 8000);
var server = http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});
