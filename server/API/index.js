var User 							= require('./User'),
	UserTopic						= require('./UserTopic'),
	Search							= require('./Search'),
	Notification					= require('./Notification'),
	Topic							= require('./Topic'),
	Socket							= require('./Socket'),
	Connection						= require('./Connection'),
	ConversationRoom				= require('./ConversationRoom'),
	Social							= require('./Social'),
	Feature							= require('./Feature'),
	ConversationRoomMember			= require('./ConversationRoomMember'),
	Profile							= require('./Profile');


module.exports = function(app) 
	{
		/*User.router(app);
		UserTopic.router(app);
		Search.router(app);
		Notification.router(app);
		Topic.router(app);
		Connection.router(app);
		ConversationRoom.router(app);
		Social.router(app);
		Feature.router(app);
		ConversationRoomMember.router(app);*/
		Profile.router(app);
	};
	