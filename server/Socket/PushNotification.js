var RedisRooms = require('../../server/redis/Rooms.js');
module.exports = {
	UpdateAdmins : function(data)
	{
		console.log('UpdateAdmins PushNotification.js file');
		redis.lrange('Room:Admins:' + data.conversation_room_id,0,1,function(error,admins){
			RedisRooms.parseArray(admins,function(parsedAdmins){
				io.sockets.in(data.conversation_room_id).emit("Room Notification",{Admins : parsedAdmins});
			});
		});
		
	},
	UpdatePeople : function(data)
	{
		console.log('UpdatePeople PushNotification.js file');
		var response = {};
		response.Users = [];
		redis.lrange('Room:Admins:' + data.conversation_room_id,2,-1,function(error,all){
		RedisRooms.parseArray(all,function(parsedAdmins){
			response.Users = parsedAdmins;//Allmessages;
			response.TotalUsers = all.length;
			io.sockets.in(data.conversation_room_id).emit("Room Notification",{People : response});
		});
	});
		
	},
	NewUserJoined : function(data)
	{
		/*socket.broadcast.to(data.conversation_room_id).emit('New User',{
			conversation_room_id : data.conversation_room_id,
            username: data.username,
            Status: 'join'
        });*/
		io.to(data.conversation_room_id).emit('New User',{
			conversation_room_id : data.conversation_room_id,
            username: data.username,
            Status: 'join'
        });
	},
	LeaveUserJoined : function(data)
	{
		/*socket.broadcast.to(data.conversation_room_id).emit('New User',{
			conversation_room_id : data.conversation_room_id,
            username: data.username,
            Status: 'join'
        });*/
		io.to(data.conversation_room_id).emit('New User',{
			conversation_room_id : data.conversation_room_id,
            username: data.username,
            Status: 'join'
        });
	}
};





