io = require('socket.io')(3000);
var usershelf = require('../../server/database/UsersShelf.js');
var Connection = require('../../server/database/ConnectionsShelf.js');
var utils = require('../../server/Socket/Utils.js');
sub = redis_store.createClient();
pub = redis_store.createClient();
io.sockets.setMaxListeners(0);
sub.subscribe('public');
//io.set('transports', ['websocket','flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling', 'polling']);
 //io.use('transports',['websocket']);
io.set('authorization', function (handshakeData, callback) {
	console.log('handshakeData request get',handshakeData._query.user_id,handshakeData._query.token)
	if(!handshakeData._query.user_id || !handshakeData._query.token) callback("user_id and Valid token must be require for the socket connection", false);
	else
	{
		redis.hget("Profile:"+handshakeData._query.user_id,handshakeData._query.token,function(erro,value){
			if(value) 
			{
				usershelf.authenticate(handshakeData._query.user_id,handshakeData._query.token,function(status) {
				console.log("socket connected");
				if(status) 
					callback(null, true);
				else callback(null, false);
				}).catch(usershelf.NotFoundError, function() {
					callback("User Not found", false);
				}).catch(function(err) {
					callback( err.message, false);
					});
			}
		else callback("User not found", false);
		});
	}	
	//callback(null, true);
});

io.on('connection', function (socket) {
	//socket.join('public');
	//console.log("hello this is connected",socket.handshake.query.user_id+"               "+socket.id);
	//utils.getPublicRoomsInfo(function(status) {});
	redis.hset('AllSockets',socket.handshake.query.user_id, socket.id);
	redis.hget('AllSockets',socket.handshake.query.user_id,function(er,value){
	//console.log("socket in redis",value);
	});
	redis.hget("Profile:"+socket.handshake.query.user_id,"room_id",function(err, joinedRoom) {
	if(joinedRoom)
	{
		socket.join(joinedRoom);
		console.log('User : '+ socket.handshake.query.user_id +'Join room on socket : '+   joinedRoom);
	}
	});
	socket.on('news', function (data) {
		socket.emit('response', { hello: 'world' });
	});
	socket.on('Create Room', function (data) {
	console.log('create room',data);
		utils.validRoomName(data, function(roomValidation) {
			if(roomValidation) utils.createRoom(data,function(obj){
			if(obj) 
			{
				//Connection.getconnectionsFollower(data.user_id,function(notify) 
				//{
				//	obj.Followers = notify;
					console.log('Create Room notify',obj);
					socket.emit('Room Created',obj );
					//pub.publish('public', obj);
					socket.join(obj.conversation_room_id);
				//});
			}
			else
			{
				socket.emit('Error',{status:"Could not Create Conversation. Please try again."});
			}
			});
		});
	
	});
    socket.on('Room User Invite', function (data) {
	redis.hgetall('AllSockets',function(errforhash,hashvalue){
		if(!errforhash && hashvalue)
		{
			data.InvitedUsers.forEach(function(val, index) {
				if(hashvalue[val])
				{	console.log('yahooooo       id : '+val + '  SocketId : '+hashvalue[val]);
					// same name for Room Notification 
					//socket.broadcast.to(hashvalue[val]).emit('Room Notification', {Message:data.Message,conversation_room_id:data.conversation_room_id});
				}
				else
				{
					console.log('user is not online id: '+val + '  SocketId : '+hashvalue[val])
				}
			});
		}
	});
    console.log('pending task',data);
	socket.emit('Room User Invited',true);
  });
  
	socket.on('User Followers', function (data) 
	{
		Connection.getconnectionsFollowings(data.user_id,function(followers) {
			socket.emit('User Followers Data',followers);
			
		});
	
	});
	socket.on('User Followings', function (data)
	{
		Connection.getconnectionsFollower(data.user_id,function(followers) {
			socket.emit('User Followings Data',followers);
		});
	});
    socket.on('Room Chat', function (data) {
		console.log('User Invite',data);
		socket.emit('Room Chat Reply',true);
	});
	socket.on('Leave Room', function (data) {
	console.log('Leave Room',data);	
		utils.LeaveRoom(data, function(status) {
			if(status)
			{
				socket.leave(data.conversation_room_id);
			}
			else socket.emit('Error',{status:"There is a problem in the request. Try again later"});
		});	
	});
	
	socket.on('New User Join', function (data) {
	socket.username = data.username;
	socket.conversation_room_id = data.conversation_room_id;
    console.log('New User Join on',data);
	utils.newUserJoinRoom(data, function(status) {
	if(status)
	{
		socket.join(data.conversation_room_id);
		socket.emit('New User Joined',status);	  
		// socket.broadcast.to(socket.conversation_room_id).emit('New User',{
			// conversation_room_id : data.conversation_room_id,
            // username: data.username,
            // Status: 'join'
        // });Room Notification
		socket.broadcast.to(socket.conversation_room_id).emit('Room Notification',{NewUser:{
			conversation_room_id : data.conversation_room_id,
            username: data.username,
            Status: 'join'
        }});
	}
    else socket.emit('Error',{status:"Try again later."});
    });
  });
  
   socket.on('All Conversations', function () {
   // console.log('All Conversations');
	utils.getPublicRoomsInfo(function(status) {
	console.log('getPublicRoomsInfo',status);
	//if(status)
	//{
		socket.emit('Return Conversations',status);
	//}
    });
	
  });
  
  socket.on('Conversation Messages', function (data) {
    console.log('Conversation Messages',data);
	utils.paginationMessages(data,function(status) {
	console.log('utils.paginationMessages',status);
	if(status) socket.emit('Return Conversation Messages',status);
	else socket.emit('Error',{status:"No more Messages"});
    });
	
  });
  socket.on('Room Participants', function (data) {
    console.log('Room Participants',data);
	utils.roomParticipants(data,function(status) {
	console.log('utils.Room Participants',status);
	if(status) socket.emit('Return Room Participants',status);
	else socket.emit('Error',{status:"Try again later."});
    });
	
  });
  socket.on('Room Tags', function (data) {
    console.log('Room Tags',data);
	utils.roomTags(function(status) {
	console.log('utils.Room Tags',status);
	if(status) socket.emit('Return Room Tags',status);
	else socket.emit('Error',{status:"Try again later."});
    });
	
  });
   socket.on('Message', function (data) {
   console.log('request New message',data);
	redis.hget("Profile:"+data.user_id,"room_id",function(err, joinedRoom) {
	console.log('New message',data,'redis room join by user : ',joinedRoom);
	if(joinedRoom && joinedRoom == data.conversation_room_id)
	{
		data.typing = false;
		data.DateTime = new Date();
		//var check = findClientsSocketByRoomId( data.conversation_room_id)
		//console.log('code from stack overflow: ',check,check.length)
		io.sockets.in(data.conversation_room_id).emit("Message Reply",data);
		redis.hincrby('rooms:' + data.conversation_room_id + ':info', 'Messages', 1,function(err,NoOfMsgs){
			data.MsgId = NoOfMsgs;
			redis.rpush('Room:Messages:'+data.conversation_room_id,JSON.stringify(data));
			utils.messageInRoom(data,function(status) {});
			utils.SaveMsgDb(data);
		});
	}
	else socket.emit('Error',{status:"You are not allowed to send message in this conversation."});
	});
	//var check = findClientsSocketByRoomId( data.conversation_room_id)
	//console.log('code from stack overflow: ',check,check.length)
	//socket.broadcast.to(socket.room).emit('Message Reply',data);
  });
  
	socket.on('User Status', function (data) {
		redis.zscore("Online",data.user_id,function(err, status) {
		console.log('User Status',status);
		if(status) socket.emit('Return User Status',{Status:status});
		else socket.emit('Return User Status',{user_status:0});
		});
	});
	
	socket.on('Edit Room', function (data) {
	console.log('Edit Room request',data);
		utils.validRoomNameEdit(data, function(roomValidation) {
			if(roomValidation) 
			{	
				utils.updateRoom(data,function(obj){
				if(obj) 
				{
					
						console.log('Edit Conversation',obj);
						io.sockets.in(data.conversation_room_id).emit('Return Edit Room',obj);
						//io.sockets.in(data.conversation_room_id).emit("Room Notification",data);
						//socket.emit('Return Edit Room',obj );
				}
				else
				{
					socket.emit('Error',{status:"Could not edit Conversation. Please try again."});
				}
				});
			}
			else
			{
				console.log('Edit Conversation else');
				socket.emit('Error',{status:"Exceed conversation subject limit."});
			}
			
		});
	
	});
	socket.on('User Typing', function (data) {
	console.log('Socket : User Typing request get ',data);
		//io.sockets.in(data.conversation_room_id).emit("Return User Typing",data);
		socket.broadcast.to(data.conversation_room_id).emit("Return User Typing",data);
	});
  // sub.on('message', function (channel, message) {
	// console.log("redis sub message",channel,message);
        // socket.emit(channel, message);
    // });
function findClientsSocketByRoomId(roomId) {
	var res = [],
		room = io.sockets.adapter.rooms[roomId];
	if (room) 
	{
		for (var id in room) {
		res.push(io.sockets.adapter.nsp.connected[id]);
		}
	}
	return res;
	}
});

