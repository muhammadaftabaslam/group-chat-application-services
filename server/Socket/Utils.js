var crypto = require('crypto')
  , type = require('component-type');
var usershelf = require('../../server/database/UsersShelf.js');
var ConversationRoomShelf = require('../../server/database/ConversationRoomShelf.js');
var ConversationRoomMemberShelf = require('../../server/database/ConversationRoomMemberShelf.js');
var FeaturesShelf = require('../../server/database/FeaturesShelf.js');
var RedisRooms = require('../../server/redis/Rooms.js');
var PushNotification = require('../../server/Socket/PushNotification.js');
var ConversationMessageShelf = require('../../server/database/ConversationMessageShelf.js');
var fs = require('fs');
var _ = require('underscore');
/*
 * Restrict paths
 
 //  1 for public
	 2 for private
 */

exports.restrict = function(req, res, next){
next();
  // if(req.isAuthenticated()) next();
  // else res.redirect('/');
};

/*
 * Generates a URI Like key for a room
 */       
exports.genRoomKey = function() {
  var shasum = crypto.createHash('sha1');
  shasum.update(Date.now().toString());
  return shasum.digest('hex').substr(0,6);
};

exports.redisSession = function(user_id,token,email) {
			redis.hset("Profile:"+user_id,token,"token");
			redis.hincrby("Profile:"+user_id,'LoggedIn',1);
			if(email)redis.hset("Profile:"+user_id,"email",email);
			redis.zadd("Online",1,user_id);
			redis.set("Profile1:"+user_id,10,"EX",3600)
};

exports.redisDelSession = function(user_id,token) {

	redis.hget("Profile:"+user_id,'LoggedIn',function(error,value){
		if(value == 1)
		{
			redis.del("Profile:"+user_id);
			redis.del("UserNotification:"+user_id);
			redis.del("Profile1:"+user_id);
		}
		else
		{
			redis.zrem("Online",user_id);
			redis.hincrby("Profile:"+user_id,'LoggedIn',-1);
			redis.hdel("Profile:"+user_id,token,function(err, userAdded) {});
			//callback(value);
		}
	
	});
};

/*
 * save message in db
 */

exports.SaveMsgDb = function(data) {
console.log('SaveMsgDb');
var obj = {
	conversation_room_id : data.conversation_room_id,
	user_id : data.user_id,
	conversation_message_text : data.MessageText,
	conversation_message_date : data.DateTime
	}
	ConversationMessageShelf.insert(obj).then(function(msgModel){
	if(msgModel) console.log('Message saved in the message tabel');
	else console.log('Message doesn not saved in the message tabel due to some issue.');
	});
	
}


/*
 * Room name is valid
 */

exports.validRoomName = function(data,fn) {
if(data.conversation_room_name)
{
  data.conversation_room_name = data.conversation_room_name.trim();
  var nameLen = data.conversation_room_name.length;
  if(nameLen < 1000 && nameLen >0) {
    fn(true);
  } else {
    fn(false);
  }
}
else fn(false);
};
exports.validRoomNameEdit = function(data,fn) {
if(data.conversation_room_name)
{
  data.conversation_room_name = data.conversation_room_name.trim();
  var nameLen = data.conversation_room_name.length;
  if(nameLen < 1000 && nameLen >0) {
    fn(true);
  } else {
    fn(false);
  }
}
else fn(true);
};
exports.roomTags = function(fn) {
	redis.zrange(Active_Topics,0,-1,function(error,activetopics){
		fn(activetopics);
	});
	
};



exports.updateRoom = function(data,fn) {
console.log('updateRoom request on socket',data);
	
	if(data.conversation_room_avatar)
	{
		console.log('conversation_room_avatar                   ',data.conversation_room_avatar);
		decodeBase64Image(data.conversation_room_avatar,function(base64Data){
			var tempstr = exports.genRoomKey();
			var newPath = publicImagePath+'/'+tempstr+".jpg";
			fs.writeFile(newPath, base64Data.data, function (err) {
				if (!err)
				{
					data.conversation_room_avatar = server_url+"/"+tempstr+".jpg";
					updateRoom(data,function(createdConversation){
						if(createdConversation) fn(createdConversation);
						else fn(false);
					});                
				}
				else
				{
					fn(false);
				}
			});
		});
	}
    else
	{
		//console.log('not conversation_room_avatar');
		updateRoom(data,function(createdConversation){
			if(createdConversation) fn(createdConversation);
			else fn(false);
		});
	}
	//room.LastActivity = new Date();
	
};

function updateRoom(room,callback)
{
	//console.log('room',room);
	var object = {};
	var obj = {};
	obj = room;
	object = _.omit(room,'user_id');
	ConversationRoomShelf.update(object,function(createdConversation){
	//console.log('createdConversation',createdConversation);
	if(createdConversation)
	{
		var id = createdConversation.id;
		var obj = createdConversation.toJSON();
		obj.conversation_room_id = id;
		obj.user_id = room.conversation_room_created_by;
		//console.log('createdConversation',obj,obj.user_id);
		//userJoinRoomDatabase(obj,function(dbresult){});
		RedisRooms.updateRoom(obj);
		callback(obj);
		
	}
	else callback(false);
	});
}
/*
 * Creates a room
 */       
function insertRoom(room,callback)
{
	console.log('room',room);
	ConversationRoomShelf.insert(room).then(function(createdConversation){
	if(createdConversation)
	{
		var id = createdConversation.id;
		var obj = createdConversation.toJSON();
		obj.conversation_room_id = id;
		obj.user_id = room.conversation_room_created_by;
		//console.log('createdConversation',obj,obj.user_id);
		//userJoinRoomDatabase(obj,function(dbresult){});
		RedisRooms.CreateRoom(obj);
		callback(obj);
		
	}
	else callback(false);
	});
}
function decodeBase64Image(dataString,callback) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};
        if (matches.length !== 3) {
            console.log('Error on the image decoding for bse 64 image');
			callback();
        }
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
        callback(response);
    } 
exports.createRoom = function(data,fn) {
console.log('createRoom request on socket',data);
	var room = {
        conversation_room_name: data.conversation_room_name,
        conversation_room_created_by: data.user_id,
		topic_id: data.topic_id,
		conversation_room_topic: data.conversation_room_topic,
		conversation_room_creation_date: new Date(),
		conversation_room_type_id: data.conversation_room_type_id
    };
	if(data.conversation_room_avatar)
	{
		//console.log('conversation_room_avatar');
		decodeBase64Image(data.conversation_room_avatar,function(base64Data){
			var tempstr = exports.genRoomKey();
			var newPath = publicImagePath+'/'+tempstr+".jpg";
			fs.writeFile(newPath, base64Data.data, function (err) {
				if (!err)
				{
					room.conversation_room_avatar = server_url+"/"+tempstr+".jpg";
					insertRoom(room,function(createdConversation){
						if(createdConversation) fn(createdConversation);
						else fn(false);
					});                
				}
				else
				{
					fn(false);
				}
			});
		});
	}
    else
	{
		//console.log('not conversation_room_avatar');
		insertRoom(room,function(createdConversation){
			if(createdConversation) fn(createdConversation);
			else fn(false);
		});
	}
	//room.LastActivity = new Date();
	
};
exports.newUserJoinRoom = function(data,fn) {
	
	redis.hget("Profile:"+data.user_id,"room_id",function(err, joinedRoom) {
		if(joinedRoom && joinedRoom == data.conversation_room_id)
		{
			console.log(' already joinedRoom in redis');
			RedisRooms.getRoomData(data,function(res){
			console.log('joinRoomInRedis',res);
			PushNotification.UpdateAdmins(data);
			PushNotification.UpdatePeople(data);
				fn(res);
			})
		}
		else
		{
			
			redis.hset('rooms:' + data.conversation_room_id + ':online', data.user_id,JSON.stringify(data),function(err, userAdded) {
			console.log('not  already joinedRoom in redis',userAdded);
			if(userAdded)
			{
				
				userJoinRoomDatabase(data,function(dbresult){
				if(dbresult)
				{
					redis.hincrby('rooms:' + data.conversation_room_id + ':info', 'OnlineUsers', 1,function(err,noofusers){});
					console.log('not  already joinedRoomdb ');
					RedisRooms.joinRoomInRedis(data,function(res){
					PushNotification.UpdateAdmins(data);
					PushNotification.UpdatePeople(data);
					//PushNotification.NewUserJoined(data);
						fn(res);
					});
				}
				else
				{
					console.log('already joinedRoom db');
					redis.hincrby('rooms:' + data.conversation_room_id + ':info', 'OnlineUsers', 1,function(err,noofusers){});
					//already joined room once and request to join again now
					RedisRooms.joinRoomInRedis(data,function(res){
					PushNotification.UpdateAdmins(data);
					PushNotification.UpdatePeople(data);
						fn(res);
					});
				}
				})
			}
			else 
			{
				RedisRooms.getRoomData(data,function(res){
				PushNotification.UpdateAdmins(data);
				PushNotification.UpdatePeople(data);
						fn(res);
					});
			}
			//fn(false);
			});
			
		}
	}); 
	
}
var userJoinRoomDatabase = function(data,fn){ 
var obj = {
	user_id:data.user_id,
	conversation_room_id:data.conversation_room_id
}
	ConversationRoomMemberShelf.insert(obj,function(user) {
	if(user)
	{
		ConversationRoomShelf.incrementMaxUsers(data.conversation_room_id).then(function(maxuser) {
			fn(true);
		});	
	}
	else 
	{
		fn(false);
	}	
	});
}
function closeRoom(data)
{
	ConversationRoomShelf.closeRoom(data.conversation_room_id).then(function(closedroom) {
		if(closedroom) RedisRooms.closeRoom(data,closedroom);
		
	});	
}
function checkTimeToCloseRoom(data)
{
	redis.llen('Room:Admins:' + data.conversation_room_id,function(error,len){					
	console.log('length',error,len);
	if(len == 0) closeRoom(data);					
	});
}
exports.LeaveRoom = function(data,fn) { 
	redis.hdel("Profile:"+data.user_id,"room_id",data.conversation_room_id);
	redis.hdel('rooms:' + data.conversation_room_id + ':online', data.user_id);
	redis.zadd("Online",1,data.user_id);		
	redis.hincrby('rooms:' + data.conversation_room_id + ':info', 'OnlineUsers', -1);
	redis.zrank('Room:Admins:ID:' + data.conversation_room_id, data.user_id,function(err, rank) {
	console.log('rank',err,rank);
		redis.zrem('Room:Admins:ID:' + data.conversation_room_id,data.user_id,function(errofzadd,dataofzadd){	
			console.log('errofzadd,dataofzadd',errofzadd,dataofzadd);		
				redis.lset('Room:Admins:' + data.conversation_room_id,rank, "-1",function(lserr,lset){
				console.log('lserr,lset',lserr,lset);
					redis.lrem('Room:Admins:' + data.conversation_room_id, 1, -1,function(lrerr,lrem){
						PushNotification.UpdateAdmins(data);
						PushNotification.UpdatePeople(data);
						console.log('lrerr,lrem',lrerr,lrem);
						checkTimeToCloseRoom(data);
						fn(data);
					
					});
				});
			});
		}); 
};
exports.messageInRoom = function(data,fn) {

	ConversationRoomShelf.incrementMsgCount(data.conversation_room_id).then(function(msgcount) {
		fn();
	});	

}
exports.paginationMessages = function(data,fn) {
console.log('paginationMessages',data);
var response = {};
response.Messages = [];
var last = data.page_id * 10;
var first = last +10;
	redis.lrange('Room:Messages:' + data.conversation_room_id,-first,-last,function(error,lastmessages){
						response.Messages = lastmessages;//Allmessages;
						response.TotalMessages = lastmessages.length;
						fn(response);
					});

}
exports.roomParticipants = function(data,fn) {
console.log('roomParticipants',data);
var response = {};
response.Users = [];

	redis.lrange('Room:Admins:' + data.conversation_room_id,2,-1,function(error,lastmessages){
		RedisRooms.parseArray(lastmessages,function(parsedAdmins){
			response.Users = parsedAdmins;//Allmessages;
			response.TotalUsers = lastmessages.length;
			fn(response);
		});
	});

}
exports.changeTopicIdToName = function(data,fn) { 
redis.hgetall('Topics', function(err, room) {
				if(!err && room)
				{
					data.forEach(function (key)
					{
						key.topic_name = room[key.topic_id]
					});
					//console.log(data)
					fn(data)
				}
				else 
				{
					fn()
				}
				
				
				});


};

exports.mergeDuplicateFeatureData = function(data,fn) { 
var tmp = {}
var obj = {}
		data.forEach(function (key)
			{
				obj[key.conversation_room_id]	 = {}
				obj[key.conversation_room_id].conversation_room_name	 = key.conversation_room_name
				obj[key.conversation_room_id]['topic_id']	 = key.topic_id
				obj[key.conversation_room_id]['topic_name']	 = key.topic_name
				obj[key.conversation_room_id].user = []
				//_.omit(key,'conversation_room_name','topic_id','topic_name')
			});
			data.forEach(function (key)
			{
				var obj1 = {}
				obj1.user_id = key.user
				obj1.user_avatar = key.user_avatar
				obj1.username = key.username
				obj[key.conversation_room_id].user.push(obj1)
				
			});
	//console.log('obj',obj)
	fn(obj)
	//console.log('data',data)
};





exports.getRoomInfo = function(req, res, redis, fn) { 
  redis.hgetall('rooms:' + req.params.id + ':info', function(err, room) {
    if(!err && room && Object.keys(room).length) fn(room);
    else res.redirect('back');
  });
};



exports.getPublicRoomsInfo = function(fn) {
	FeaturesShelf.landingPageConversation(function(landingPageConversation){
		FeaturesShelf.landingPageUsersInConversation(function(landingPageUsersInConversation){
			
			mergeLandingPageData(landingPageConversation,landingPageUsersInConversation,function(data){
				redis.zcard('Active_Topics',function(tperror,topicslen){
				data.Tags = topicslen;
					fn(data);
				});
	
			})
	
		})
	
	
	})
	
	
};




var mergeLandingPageData = function(landingPageConversation,landingPageUsersInConversation,fn) { 


		landingPageConversation.forEach(function (key)
		{
			key.users = [];
			key.room_last_activity = new Date().toString();
			landingPageUsersInConversation.forEach(function (key1)
			{
				key.conversation_room_id;
				if(key.conversation_room_id == key1.conversation_room_id)
				{
					
						key.users.push(key1);
					
				}
					
			});
		});
		var data = {};
		data.Rooms = landingPageConversation;
        data.TotalConversations = landingPageConversation.length;
		redis.zrange('Online',0,-1,'WITHSCORES' ,function(err, TotalOnlineUser) {
            data.TotalOnlineUsers = TotalOnlineUser.length;
			fn(data)
        });
	//console.log('landingPageConversation',landingPageConversation[0].users);	
	//fn(landingPageConversation)
	
};





exports.getPublicRoomsInfo1 = function(fn) {

	redis.smembers('convo:public:rooms', function(err, publicRooms) {
	//console.log('length',publicRooms.length);
	var info = {};
    var rooms = [];
	var convousers = [];
	info.TotalConversations = publicRooms.length;
	
		redis.zrange('Online',0,-1,'WITHSCORES' ,function(err, TotalOnlineUser) {
            info.TotalOnlineUsers = TotalOnlineUser.length;
            
		var len = publicRooms.length;
		if(!len) fn([]);

		publicRooms.sort(exports.caseInsensitiveSort);
		
		publicRooms.forEach(function(roomKey, index) {
		//redis.smembers('rooms:' + roomKey + ':online', function(err, users) {
		//console.log("Room users  " + roomKey +'  -----    '+ users);
		redis.hgetall('rooms:' + roomKey + ':online',function(errforhash,hashvalue){
		//hashvalue = hashvalue.slice(0, 4);
		//console.log('utils file for room members',hashvalue)
		var UsersOfRooms = [];
		redis.hmget('rooms:' + roomKey + ':info','key','Title','Admin','topic_id','topic_name','Type','CreationDate','LastActivity','OnlineUsers', function(err, room) {
        for (var key in hashvalue) {
			if (hashvalue.hasOwnProperty(key)) {
			var val = JSON.parse(hashvalue[key]);
			UsersOfRooms.push(val);
			//console.log('val',val);
			}
		}
		//console.log("hashvalue",hashvalue);
		UsersOfRooms = UsersOfRooms.slice(0, 5);
		//console.log('room',room);// prevent for a room info deleted before this check
        if(!err && room && Object.keys(room).length) {
          // add room info
          rooms.push({
			key: room[0],
			Title: room[1],
			Admin: room[2],
			topic_id: room[3],
			topic_name: room[4],
			Type: room[5],
			CreationDate: room[6],
			LastActivity: room[7],
			TotalUsers: room[8],
			ConversationUsers: UsersOfRooms
          });
          if(rooms.length == len) 
		  {
			info.Rooms = rooms
			fn(info);
			}
        } else {
          // reduce check length
          len -= 1;
        }
      });
	  });
	  }); 
    });
  });
};
/*
 * Get connected users at room
 */

exports.getUsersInRoom = function(req, res, redis, room, fn) {
  redis.smembers('rooms:' + req.params.id + ':online', function(err, online_users) {
    var users = [];

    online_users.forEach(function(userKey, index) {
      redis.get('users:' + userKey + ':status', function(err, status) {
        var msnData = userKey.split(':')
          , username = msnData.length > 1 ? msnData[1] : msnData[0]
          , provider = msnData.length > 1 ? msnData[0] : "twitter";

        users.push({
            username: username,
            provider: provider,
            status: status || 'available'
        });
      });
    });

    fn(users);

  });
};

/*
 * Get public rooms
 */

exports.getPublicRooms = function(redis, fn){
  redis.smembers("convo:public:rooms", function(err, rooms) {
    if (!err && rooms) fn(rooms);
    else fn([]);
  });
};
/*
 * Get User status
 */

exports.getUserStatus = function(userid, fn){
  redis.zscore('Online',userid, function(err, status) {
    if (!err && status) fn(status);
    else fn(false);
  });
};
exports.setUserStatus = function(userid,value,fn){
  redis.zadd('Online',value,userid,function(err, status) {
    if (!err && status) fn(status);
    else fn(false);
  });
};

/*
 * Enter to a room
 */

exports.enterRoom = function(req, res, room, users, rooms, status){
  res.locals({
    room: room,
    rooms: rooms,
    user: {
      nickname: req.user.username,
      provider: req.user.provider,
      status: status
    },
    users_list: users
  });
  res.render('room');
};

/*
 * Sort Case Insensitive
 */

exports.caseInsensitiveSort = function (a, b) { 
   var ret = 0;

   a = a.toLowerCase();
   b = b.toLowerCase();

   if(a > b) ret = 1;
   if(a < b) ret = -1; 

   return ret;
};

/**
 * Merge object `b` into `a`.
 *
 * @param {Object} a
 * @param {Object} b
 * @return {Object} a
 * @api public
 */

exports.merge = function merge(a, b) {
  for (var key in b) {
    if (exports.has.call(b, key) && b[key]) {
      if ('object' === type(b[key])) {
        if ('undefined' === type(a[key])) a[key] = {};
        exports.merge(a[key], b[key]);
      } else {
        a[key] = b[key];
      }
    }
  }
  return a;
};

/**
 * HOP 
 */

exports.has = Object.prototype.hasOwnProperty;
