var Connection = require('../../server/database/ConnectionsShelf.js');
var FeaturesShelf = require('../../server/database/FeaturesShelf.js');
var usershelf = require('../../server/database/UsersShelf.js');
module.exports = function(app) {
	app.get('/convo/connection/getfollowing/:id', function(req, res) {
	if(debugModeOn) console.log('getfollowing : ConnectionCtrl : Request get : ', req.body);
	Connection.getconnectionsFollowings(req.params.id,function(notify) {
			if(debugModeOn) console.log('getfollowing : ConnectionCtrl : return request from getconnectionsFollowings : ', notify);
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:'No result'});
			});
		
	});
	app.get('/convo/connection/getfollower/:id', function(req, res) {
	Connection.getconnectionsFollower(req.params.id,function(notify) {
				if(debugModeOn) console.log('getconnectionsFollower : ConnectionCtrl : return request from addConnection : ', notify);
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:'No result'});
			});		
	});

	app.post('/convo/connection/getfollowing', function(req, res) {
	if(debugModeOn) console.log('getfollowing : ConnectionCtrl : Request get : ', req.body);
	if (!req.body.user_id || !req.body.token) res.json(404, {status: "UserId and Token are required"});
	usershelf.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			Connection.getconnectionsFollowings(req.body.user_id,function(notify) {
			if(debugModeOn) console.log('getfollowing : ConnectionCtrl : return request from getconnectionsFollowings : ', notify);
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:'No result'});
			});	
		}
		else res.json(401, {error: "Authentication failed"});
	}).catch(usershelf.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
		
	});
	app.post('/convo/connection/addconnection', function(req, res) {
		if(debugModeOn) console.log('addconnection : ConnectionCtrl : Request get : ', req.body);
		if (!req.body.user_id || !req.body.token) res.json(404, {status: "UserId and token are required"});
		if(!req.body.data) res.json(404, {error: "Add Connections data must be an array with mentioned key value formats"});
		usershelf.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			Connection.addConnection(req.body.data,function(notify) {
			if(debugModeOn) console.log('addConnection : ConnectionCtrl : return request from addConnection : ', notify);
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:'No result'});
			});
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(usershelf.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	app.post('/convo/connection/getfollower', function(req, res) {
	if(debugModeOn) console.log('getfollower : ConnectionCtrl : Request get : ', req.body);
		if (!req.body.user_id || !req.body.token) res.json(404, {status: "user_id and token are required"});
		usershelf.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			Connection.getconnectionsFollower(req.body.user_id,function(notify) {
				if(debugModeOn) console.log('getconnectionsFollower : ConnectionCtrl : return request from addConnection : ', notify);
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:'No result'});
			});	
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(usershelf.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
		res.json(500, {error: err.message});
		});
		
	});
	app.get('/convo/connection/friendslist/:id', function(req, res) {
	
		
		
			FeaturesShelf.friends(req.params.id,function(notify) {
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:'No result'});
			});	
	
		
	});
	app.post('/convo/connection/friendslist', function(req, res) {
	
		if(debugModeOn) console.log('friendslist : UserCtrl : Request get : ', req.body);
		if (!req.body.user_id || !req.body.token) res.json(404, {status: "user_id and token are required"});
		usershelf.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			FeaturesShelf.friends(req.body.user_id,function(notify) {
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,notify);
				else res.json(200, {error:{}});
			});	
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(usershelf.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	app.post('/convo/connection/removeconnection', function(req, res) {
		if(debugModeOn) console.log('removeConnection : ConnectionCtrl : Request get : ', req.body);
		if (!req.body.user_id || !req.body.token) res.json(404, {status: "UserId and token are required"});
		if(!req.body.connection_id) res.json(404, {error: "Remove Connections data must have a user need to unfollow"});
		usershelf.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			var obj = {
				connection_user_id:req.body.user_id,
				user_id:req.body.connection_id
			}
			Connection.removeConnection(obj,function(notify) {
			if(debugModeOn) console.log('removeConnection : ConnectionCtrl : return request from removeConnection : ', notify);
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(200);
				else res.json(200);
			});
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(usershelf.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	
};