var ConversationRoom = require('../../server/database/ConversationRoomShelf.js');
var usershelf = require('../../server/database/UsersShelf.js');
var ConversationRoomMember = require('../../server/database/ConversationRoomMemberShelf.js');
var utils = require('../../server/controllers/utils.js');
module.exports = function(app) {


	app.post('/convo/room/create', function(req, res) {
	var data = req.body;
	if (!req.body.UserId || !req.body.Token) res.json(404, {status: "UserId and Token are required"});
	var room = {
		ConversationRoomName : data.Title,
		//ConversationRoomUrl :
		ConversationRoomCreatedBy : data.UserId,
		ConversationRoomTypeId : data.Type,
		ConversationRoomTopicId: data.TopicId,
		ConversationRoomCreationDate: new Date(),
		ConversationRoomMaxUsers : 1
		//ConversationRoomTopic : data.Topic
	}
	var room_member = {
		ConversationRoomMemberUserId : data.UserId,
		ConversationRoomJoiningDate : new Date(),
		ConversationRoomMembeIsAdmin : 1
		}
		
	//console.log('Rooms info by Kashif',room);	
	//console.log('Rooms-member info by Kashif',room_member);	
	usershelf.authenticate(req.body.UserId,req.body.Token,function(status) {
	//console.log('authentication',status);
		if(status)
		{
			
			ConversationRoom.insert(room,function(roomreply) {
			//console.log('ConversationRoom',roomreply);	
				if (typeof roomreply !== 'undefined')
				{
					console.log('roomreply',roomreply);
					room_member.ConversationRoomMemberRoomId = roomreply.ConversationRoomId;
						ConversationRoomMember.insert(room_member,function(memreply) {
						//console.log('ConversationRoomMember',memreply);	
						//{Roomkey:roomKey}
							res.json(202,{Roomkey:roomreply.ConversationRoomId});
						});
				}
				else res.json(501, {error:'No result'});
			});	
		}
		else res.json(401, {error: "Authentication failed"});
	}).catch(usershelf.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
		
	});
	app.post('/convo/ConversationRoom/addConversationRoom', function(req, res) {
		if (!req.body.UserId || !req.body.Token) res.json(404, {status: "UserId and Token are required"});
		if(!req.body.data) res.json(404, {error: "Add ConversationRooms data must be an array with mentioned key value formats"});
		usershelf.authenticate(req.body.UserId,req.body.Token,function(status) {
		if(status)
		{
			ConversationRoom.addConversationRoom(req.body.data,function(notify) {
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(202,notify);
				else res.json(501, {error:'No result'});
			});
			
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(usershelf.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	app.post('/convo/ConversationRoom/getfollower', function(req, res) {
		if (!req.body.UserId || !req.body.Token) res.json(404, {status: "UserId and Token are required"});
		usershelf.authenticate(req.body.UserId,req.body.Token,function(status) {
		if(status)
		{
			ConversationRoom.getConversationRoomsFollower(req.body.UserId,function(notify) {
				if (typeof notify !== 'undefined' && notify.length > 0) res.json(202,notify);
				else res.json(501, {error:'No result'});
			});
			
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(usershelf.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
		res.json(500, {error: err.message});
		});
		
	});
	
	app.get('/convo/room/testing/:id', function(req, res) {
		console.log('hello',req.params.id);
		ConversationRoom.incrementMaxUsers(req.params.id).then(function(user) {});
		res.json(200);
		
	});
	app.get('/convo/conversation/room/:id', function(req, res) {
		console.log('/convo/conversation/room/:id',req.params.id);
		utils.ConversationLink(req.params.id,function(data){
		console.log('/convo/conversation/room/:id',data);
			res.json(200,{Conversation:data});
		})
		
		
	});
};
















		
// module.exports = {
    // ConversationURL: function(req, res) {
	
		
    // }
// };