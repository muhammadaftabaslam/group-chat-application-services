var ConversationRoom = require('../../server/database/ConversationRoomShelf.js');
var usershelf = require('../../server/database/UsersShelf.js');
var ConversationRoomMember = require('../../server/database/ConversationRoomMemberShelf.js');
module.exports = function(app) {


	app.get('/convo/room/conversationusers/:conversation_room_id', function(req, res) {
	console.log('req.params.conversation_room_id',req.params.conversation_room_id)
	if (!req.params.conversation_room_id) res.json(404, {status: "Conversation Id is required"});
	else
	{
		
						ConversationRoomMember.conversationUsers(req.params.conversation_room_id,function(memreply) {
						
							res.json(202,{Users:memreply});
						});
	}	
			
		
	})
	
};