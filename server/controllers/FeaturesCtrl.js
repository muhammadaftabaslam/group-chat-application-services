var Users = require('../../server/database/UsersShelf.js');
var UserSession = require('../../server/database/SessionShelf.js');
var ConversationRoom = require('../../server/database/ConversationRoomShelf.js');
var TopicShelf = require('../../server/database/TopicShelf.js');
var FeaturesShelf = require('../../server/database/FeaturesShelf.js');
var utils = require('../../server/controllers/utils.js');

module.exports = function(app) {
	
	
	app.get('/convo/featureConversations', function(req, res) {
	console.log('featureConversations')
	FeaturesShelf.featuresConversation(function(data){
	
		utils.changeTopicIdToName(data,function(data1){
		//console.log('changeTopicIdToName',data1)
			utils.mergeDuplicateFeatureData(data1,function(last){
			
			//console.log('featureConversations',last)
			res.json(200,{Feature:last})
			})
		})
	})
		
	 
	});

	app.get('/convo/topTopics', function(req, res) {
	FeaturesShelf.topTopics(function(data){
	
		utils.changeTopicIdToName(data,function(data1){
			console.log('topTopics',data1)
			res.json(200,{Topics:data1})
			})
		})
	});
	app.post('/convo/conversation/topicname', function(req, res) {
	if(!req.body.keyword) res.json(404, {error: "Please provide proper search text"});
		FeaturesShelf.convoByTopics(req.body.keyword,function(data){
		if(data)
		{
			//console.log('conversationByTopics',data)
			res.json(200,{Topics:data})
		}
		else
		{
			res.json(400,{Topics:[]})
		}
			
		})
	
	 
	 
	});
	
	
	

	};
	