var UserNotification = require('../../server/database/UserNotificationsShelf.js');
var Users_Sessions = require('../../server/database/UsersShelf.js');
var _ = require('underscore');

module.exports = function(app) {
	app.get('/convo/notify/:id', function(req, res) {

	var id = req.params.id;//UserNotification.query({where: {user_id:user_id}}).fetchAll()
	UserNotification.query({where: {user_id:id}}).fetchAll()
		.then(function(users) {
		if(users)  res.json(200,users);//res.send(users.toJSON());
		else res.json(200, {error: 'not found'});
		}).catch(function(error) {
		  res.json(200, {error: error});
		});
	});
	app.post('/convo/usernotify/get', function(req, res) {
	if(req.body == undefined ) res.json(412, {error: "Precondition failed"});
	if(!req.body.user_id || !req.body.token) res.json(404, {error: "user_id and token both are required"});
	else Users_Sessions.authenticate(req.body.user_id,req.body.token,function(status) {
	if(status) 
	{
		UserNotification.get({user_id:req.body.user_id},function(notify){
		if (typeof notify !== 'undefined' && notify.length > 0)
		{
			Users_Sessions.getUser(req.body.user_id).then(function(users) {
			var settings = {};
			//var tempuser = users.omit('password','user_salt','user_geo_location')
			var tempuser = users.omit('user_gender','password','user_salt','user_bio','user_city','user_state','user_is_active','user_hide_conv','user_avatar','user_country');
			settings.NotificationTypes = notify;
			settings.user = tempuser;
			console.log('settings',settings);
				res.json(200,{Setting:settings});
			});
			}
		else res.json(501, {error:'No result'});
		});
	}
	else res.json(401, {error: "Authentication failed"});
	}).catch(UserNotification.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});

	app.post('/convo/usernotify/insert', function(req, res) {
	if(debugModeOn) console.log('insert : UserNotificationCtrl : Request get : ',req.body.user_id,req.body.data);
	if(req.body == undefined ) res.json(412, {error: "Precondition failed"});
	if(!req.body.user_id || !req.body.token) res.json(404, {error: "user_id and token both are required"});
	if(!req.body.data) res.json(404, {error: "Notification data must be an array with mentioned key value formats"});
	
	Users_Sessions.authenticate(req.body.user_id,req.body.token,function(status) {
	if(status) 
	{
		UserNotification.insert(req.body.user_id,req.body.data,function(notify){
		if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,{status:'Updated'});
		else res.json(200, {error:'No result'});
		});
	}
	else res.json(401, {error: "Authentication failed"});
	}).catch(UserNotification.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	
	
	
	};