var Users_Shelf = require('../../server/database/UsersShelf.js');
var User_Connections = require('../../server/database/ConnectionsShelf.js');
var Conversation = require('../../server/database/ConversationRoomShelf.js');
var UserTopic = require('../../server/database/UserTopicShelf.js');
var Features = require('../../server/database/FeaturesShelf.js');
var utils = require('../../server/controllers/utils.js');	
module.exports = {
    UserProfile: function(req, res) {
	var id = req.params.id;
	var obj = {};
		Users_Shelf.getUser(id).then(function(user) {
		
		User_Connections.checkConnection({user_id:2157,connection_user_id:id},function(connected_users){
		obj.UserFollowed = connected_users;
			if(user)
			{
			
				obj.User = user.omit('password','user_salt','user_email_backup');
				redis.zscore("Online",id,function(error,result){
				obj.UserStatus = result;
					if(result == 1)
					{
						//console.log('Visitor is Online',result);
						User_Connections.getconnectionsFollower(id,function(notify) {
							obj.Followers = notify;
							User_Connections.getconnectionsFollowings(id,function(notify1) {
								obj.Followings = notify1;
									Conversation.userConversation(id).then(function(Converstions) {
										obj.ConverstionsCreatedByUser = Converstions;
										Features.userInvolveInConversation(id,function(InvolvedConversation) {
											obj.UserConversations = InvolvedConversation;
											UserTopic.get(id,function(user_topics){
												obj.UserTopics = user_topics
													res.json(200,{status:obj});
											})
										});
									});
								});	
							});
					}
					else if(result == 2)
					{
						//console.log('Visitor is busy',result);
						utils.UserCurrentConversation(id,function(conversation) {
						obj.Conversation = conversation;
						User_Connections.getconnectionsFollower(id,function(notify) {
							obj.Followers = notify;
							User_Connections.getconnectionsFollowings(id,function(notify1) {
								obj.Followings = notify1;
									Conversation.userConversation(id).then(function(Converstions) {
										obj.ConverstionsCreatedByUser = Converstions;
										Features.userInvolveInConversation(id,function(InvolvedConversation) {
											obj.UserConversations = InvolvedConversation;
											UserTopic.get(id,function(user_topics){
												obj.UserTopics = user_topics
													res.json(200,{status:obj});
											})
										});
									});
								});	
							});
						});	
							
						
					}
							
					else
					{
						//console.log('Visitor is Offline',result);
						User_Connections.getconnectionsFollower(id,function(notify) {
							obj.Followers = notify;
							User_Connections.getconnectionsFollowings(id,function(notify1) {
								obj.Followings = notify1;
									Conversation.userConversation(id).then(function(Converstions) {
										obj.ConverstionsCreatedByUser = Converstions;
										Features.userInvolveInConversation(id,function(InvolvedConversation) {
											obj.UserConversations = InvolvedConversation;
											UserTopic.get(id,function(user_topics){
												obj.UserTopics = user_topics
													res.json(200,{status:obj});
											})
										});
									});
								});	
							});
					}
				});
			}
			else res.json(200,{Error:"User does not exist"});
		});
	});	
		
		
    }
};