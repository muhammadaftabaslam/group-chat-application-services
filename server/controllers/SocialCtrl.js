var ConversationRoom = require('../../server/database/ConversationRoomShelf.js');
var usershelf = require('../../server/database/UsersShelf.js');
var socialshelf = require('../../server/database/SocialShelf.js');
var bcrypt   = Promise.promisifyAll(require('bcrypt'));
var jwt = require('jwt-simple');
var path = require('path');
var secret = 'xxx';
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var fs = require('fs');
var salt = "";
var utils = require('../../server/controllers/utils.js');
bcrypt.genSalt(10, function(err, salt1) {
salt = salt1;
});
var UserSession = require('../../server/database/SessionShelf.js');
module.exports = function(app) {

	var dataForUserTable = Promise.method(function(obj) {
		if(obj.type == 'facebook')
		{
			var data = 
			{
				user_email : obj.user_email,
				user_first_name:obj.user_first_name,
				user_gender : obj.user_gender,
				password:'maavratech12558',
				user_last_name: obj.user_last_name,
				username:   obj.username, //name
				user_avatar : obj.user_avatar
					  
			}
			if(obj.user_bio)
			{
				data.user_bio = obj.user_bio;
			}
			return data;
		}
		else if(obj.type == 'google')
		{
			var data = 
			{
				user_email : obj.user_email,
				user_first_name:obj.user_first_name,//given name
				user_gender : obj.user_gender,
				password:'maavratech12558',
				user_last_name: obj.user_last_name,//family name
				username:   obj.username,//name  
				user_avatar : obj.user_avatar
					  
			}
			if(obj.user_bio)
			{
				data.user_bio = obj.user_bio;
			}
			return data;
		}
		else if(obj.type == 'twitter')
		{
			var data = 
			{
				
				user_avatar : obj.user_avatar,
				username : obj.username,
				password:'maavratech12558'
			}	
			if(obj.user_bio)
			{
				data.user_bio = obj.user_bio;
			}
				return data;			
		}
		else
		{
			console.log('Facebook,Twitter,Google')
		}
	});
	
	var dataForTwitterTable = Promise.method(function(social,user) {
		//console.log('socialassadasdsdr',social,user);
		var data = 
			{
				social_id : social.id,
				username:user.attributes.username,  
				avatar : social.picture,
				user_id:user.attributes.user_id,
				type : social.type
					  
			}
			console.log('dataForTwitterTable ',data);
			return data;
		
	});
	var dataForSocialTable = Promise.method(function(social,user) {
		//console.log('socialassadasdsdr',social,user);
		var data = 
			{
				social_id : social.id,
				email : user.attributes.user_email,
				first_name : user.attributes.user_first_name,
				last_name: user.attributes.user_last_name,
				username:   user.attributes.username,  
				avatar : social.picture,
				user_id:user.attributes.user_id,
				type : social.type
					  
			}
			console.log('dataForSocialTable',data.social_id);
			return data;
		
	});
	app.post('/convo/user/social/login', function(req, res) {
	console.log("/convo/user/social/login",req.body.id);
	
	if(!req.body) res.json(404, {error: "Data format must be according to API mentioned formats"});
	else if(!req.body.id)
	{	
	//console.log('req.body.id ==  undefined')
		res.json(404, {error: "There is a problem in Social login. Please try again."});
	}	
	else if(!req.body.type || req.body.type == "") res.json(404, {error: "Social Media is required"});
	else if(req.body.type == "twitter")
	{
		twitterFunction(req,res);
		
	}
	else
	{
		socialshelf.check(req.body.id).then(function(status) {
		if(status)
		{
			//console.log('user exist in social table',status)
			UserSession.checkSessionExist(status.attributes.user_id).then(function(UserSes) {
			if(UserSes)
			{
				//console.log('user is already logged in')
				usershelf.getUser(status.attributes.user_id).then(function(user) {
					user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
					UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
							utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
							//console.log('already logged in',user)
							user.attributes.user_status = false;
							res.json(200,user.omit('password','user_salt'));
						
						});
				})
				//res.json(1001,{error:"User already logged in"})
		
				
			}
			else
			{
				console.log('user is not logged in')
				usershelf.getUser(status.attributes.user_id).then(function(user) {
					user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
					UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
							utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
							user.attributes.user_status = false;
							res.json(200,user.omit('password','user_salt'));
						
						});
				})
			}
		});
			
			
		}
		else
		{
			//console.log('User is not exist in social table',status)
			usershelf.checking(req.body.user_email).then(function(checkUserInUserShelf) {
			if(checkUserInUserShelf)
			{
				res.json(500,{error:"How is it possible that user is not in social but in user ?"})
			
			}
			else
			{
				console.log('User is exist in user table')
				dataForUserTable(req.body).then(function(dataforusers) {
					usershelf.register(dataforusers).then(function(user) {
					if(user)
					{
						//console.log('social',user)
						//emailNotify.registerNotification(user.attributes);
						user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
						UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
							dataForSocialTable(req.body,user).then(function(dataforsocial) { 
							//console.log('dataForSocialTable11',dataforsocial);
								socialshelf.register(dataforsocial).then(function(socialuser) {
								//console.log('socialuser2121',socialuser);
									utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
									user.attributes.user_status = true;
										res.json(200,user.omit('password','user_salt'));
								});
							})
						});
					}
						else
						{
								res.json(404, {error: "Data format must be according to API mentioned formats"});
								
						}
					})
				})
			}
			}).catch(function(err) {
				console.log("err in the duplicate problem : ",err)
				res.status(404).json({error: err.message});
			});
		}
		}).catch(function(err) {
		console.log("err : ",err)
			res.status(404).json({error: err.message});
			});
		
		
		
		
		
		
		
	}	
	
	
	
	

	});
	app.get('/convo/user/sociallogin/:id', function(req, res) {
		console.log('req',req.params.id);
		socialshelf.check(req.params.id).then(function(status) {
		console.log(status)
			if(status)
			{
				console.log('if',status)
			}	
		});
		res.json(200)
	
	});


	function twitterFunction(req, res) {
	console.log("/convo/user/social/twitter",req.body,req.body.id);
	if(!req.body) res.json(404, {error: "Data format must be according to API mentioned formats"});
	if(!req.body.id) res.json(404, {error: "There is a problem in Social login. Please try again."});
	
	socialshelf.check(req.body.id).then(function(status) {
	if(status)
	{
		console.log('twitter status',status)
		
		usershelf.getUser(status.attributes.user_id).then(function(user) {
			user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
			UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
				utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
				user.attributes.user_status = false;
				res.json(200,user.omit('password','user_salt'));
				});
		})
		
		
		
	}
	else
	{
		console.log('else',status)
		dataForUserTable(req.body).then(function(dataforusers) {
		//console.log('dataforusers',dataforusers)
		
		
		
				
			usershelf.register(dataforusers).then(function(user) {
			if(user)
			{
				//console.log('social',user)
				//emailNotify.registerNotification(user.attributes);
				user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
				UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
					dataForTwitterTable(req.body,user).then(function(dataforsocial) { 
					//console.log('dataForSocialTable',dataforsocial);
						socialshelf.register(dataforsocial).then(function(socialuser) {
						
							utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
							user.attributes.user_status = true;
							res.json(200,user.omit('password','user_salt'));
						});
					})
				});
				}
				else
				{
					res.json(404,{error:"Email is already exist"})
				}
			})
		})
	}
	}).catch(function(err) {
		console.log("errrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr : ",err)
		res.status(404).json({error: err.message});
	});

	};

	
	app.post('/convo/user/socialLists', function(req, res) {
		console.log('/convo/user/socialLists',req.body);
		console.log('/convo/user/socialLists length',req.body.data.length);
		var data = {};
		data.InConvoLounge = []
		data.OutConvoLounge = req.body.data
		res.json(200,data)
	
	});











};