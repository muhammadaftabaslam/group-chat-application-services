var bcrypt   = Promise.promisifyAll(require('bcrypt'));
var jwt = require('jwt-simple');
var path = require('path');
var secret = 'xxx';
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var fs = require('fs');
var _ = require('underscore');
var salt = "";
var utils = require('../../server/controllers/utils.js');
//var emailNotify = require('../../server/notification/emailNotification.js');

bcrypt.genSalt(10, function(err, salt1) {
salt = salt1;
});
var Users = require('../../server/database/UsersShelf.js');
var UserSession = require('../../server/database/SessionShelf.js');
module.exports = function(app) {
	app.get('/convo/alluser', function(req, res) {
	 new Users().fetchAll()
		.then(function(users) {
		  res.send(users.toJSON());
		}).catch(function(error) {
		  console.log(error);
		  res.send(200,{Error:'An error occured'});
		});
	});
	
	app.get('/convo/user/:id', function(req, res) {
	var id = req.params.id;
	 new Users({'user_id': id}).fetch().then(function(users) {
		if(users)  res.json(200,users.omit('password'));//res.send(users.toJSON());
		else res.json(200, {error: 'not found'});
		}).catch(function(error) {
		  res.json(200, {error: error});
		});
	});

	app.post('/convo/user/login', function(req, res) {
	console.log("login",req.body);

	if(!req.body.user_email || !req.body.password || req.body == undefined) res.json(404, {error: "Email and Password both are required"});
	if(debugModeOn) console.log('Login request get with Email :'+req.body.user_email + 'Password : '+ req.body.password);
	Users.login(req.body.user_email, req.body.password).then(function(user) {
		user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
		if(debugModeOn) console.log('Login : UserCtrl : Return request from UserShelf : ', user);
		UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
		if(debugModeOn) console.log('Login : UserCtrl : Return request from SessionShelf : '+ session);
		utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
		
			if (user.get('ShowLoc') == 0) res.json(200,user.omit('password','user_salt','user_geo_location'));
			else res.json(200,user.omit('password','user_salt'));

		});
	}).catch(Users.NotFoundError, function() {
		res.status(404).json({error: req.body.user_email + ' not found'});
	}).catch(function(err) {
		res.status(404).json({error: err.message});
		});
	});
	
	app.post('/convo/user/register', function(req, res) {
	if(debugModeOn) console.log('Register request get :',req.body);
	if(!req.body.user_email || !req.body.password || req.body == undefined) res.json(404, {error: "Email and Password both are required"});
	
	Users.register(req.body).then(function(user) {
		//emailNotify.registerNotification(user.attributes);
		user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
		if(debugModeOn) console.log('Register : UserCtrl : Return request from UserShelf : '+ user);
		UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
		if(debugModeOn) console.log('Register : UserCtrl : Return request from UserShelf : '+ session);
		utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
			res.json(200,user.omit('password','user_salt'));
			
		
		});
	}).catch(Users.NotFoundError, function() {
		res.json(404, {error: req.body.Email + ' not found'});
	}).catch(function(err) {
		 if (/^ER_DUP_ENTRY: Duplicate entry/.test(err.message)) res.json(404, {error: 'Email OR Username Duplicate'});
        else res.json(404, {error: err.message});
		});
	});
	
	
	app.post('/convo/user/upload',multipartyMiddleware, function(req, res) {
	  if(debugModeOn) console.log('Image upload request get :',req.body);
	  if(!req.body.user_id || !req.body.token || req.body == undefined) res.json(404, {error: "UserId and Token both are required"});
	  Users.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			
			
			if(req.body.user_bio)
			{
				if(debugModeOn) console.log('Image upload : UserCtrl : Bio exist? : ',req.body.user_bio);
				Users.update({user_id:req.body.user_id,user_bio:req.body.user_bio}).then(function(user) {
					if(debugModeOn) console.log('Image upload : UserCtrl : Usershelf update call return for bio : ', user);
				});
			}
			if(req.body.user_avatar)
			{
				if(debugModeOn) console.log('Image upload : UserCtrl : Image Url exist? : ', req.body.user_avatar);
				Users.update({user_id:req.body.user_id,user_avatar:req.body.user_avatar}).then(function(user) {
				if(debugModeOn) console.log('Image upload : UserCtrl : Usershelf update call return for user avatar image : ', user);
				
					res.json(200, {Path:req.body.user_avatar});
				});
			} else if(req.files != 'undefined')
			{
				var file = req.files.files;
				if(debugModeOn) console.log('Image upload : UserCtrl : File Exist? : ',file.type);
				if(file != undefined)
				{
					if(file.type == "image/png" || file.type == "image/jpeg")
					{
						fs.readFile(file.path, function (err, data) {
							var newPath = app.get('/public')+'/'+req.body.user_id+".jpg";
							fs.writeFile(newPath, data, function (err) {
										Users.update({user_id:req.body.user_id,user_avatar:server_url+"/"+req.body.user_id+".jpg"}).then(function(user) {});
							});
							res.json(200, {Path:server_url+"/"+req.body.user_id+'.jpg'});
						});
					}
					else
					{
						res.json(200, {error:"Please upload image of type .png or .jpg"});
					}
				}
				else
				{
					res.json(200,{status:req.body.user_id})
				}
			} 
			else
			{
				res.json(200,{status:req.body.user_id})
			}
		}
		else res.json(401, {error: "Authentication failed"});
	}).catch(Users.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	
	
	});
	app.get('/convo/user/image/:id', function (req, res) {
		if(!req.params.id) res.json(404, {error: "Must mention UserId"});
		path.exists(app.get('/public')+'/'+req.params.id+'.jpg', function(exists) { 
		  if (exists) res.sendfile(app.get('/public')+'/'+req.params.id+'.jpg');
		  else res.json(400, {status: "Profile Pic not found"});
		}); 
	});
	// app.get('/:id', function (req, res) {
		// if(!req.params.id) res.json(404, {error: "Must mention UserId"});
		// path.exists(app.get('/public')+'/'+req.params.id+'.jpg', function(exists) { 
		  // if (exists) res.sendfile(app.get('/public')+'/'+req.params.id+'.jpg');
		  // else res.json(400, {status: "Profile Pic not found"});
		// }); 
	// });
	app.post('/convo/user/register', function(req, res) {
	console.log('register call',req.body)
	if(!req.body.user_email || !req.body.password || req.body == undefined) res.json(404, {error: "Email and Password both are required"});
	
	Users.register(req.body).then(function(user) {
		//emailNotify.registerNotification(user.attributes);
		user.attributes.token = jwt.encode(new Date().toUTCString(), secret);
		UserSession.addSession({user_id:user.get('user_id'),token:user.attributes.token}).then(function(session){
		utils.redisSession(user.get('user_id'),user.attributes.token,user.attributes.user_email);
			res.json(200,user.omit('password','user_salt'));
			
		
		});
	}).catch(Users.NotFoundError, function() {
		res.json(404, {error: req.body.Email + ' not found'});
	}).catch(function(err) {
		 if (/^ER_DUP_ENTRY: Duplicate entry/.test(err.message)) res.json(404, {error: 'Email OR Username Duplicate'});
        else res.json(404, {error: err.message});
		});
	});
	
	
	app.post('/convo/user/check', function(req, res) {
	Users.check(req.body).then(function(user) {
    res.json(user.omit('password'));
	}).catch(Users.NotFoundError, function() {
		res.json(200, {error: req.body.user_email + ' not found'});
	}).catch(function(err) {
		 if (/^ER_DUP_ENTRY: Duplicate entry/.test(err.message)) res.json(200, {error: req.body.user_email + ' Duplicate'});
        else res.json(200, {error: err});
		});
	});
	app.post('/convo/user/resetpassword', function(req, res) {
	console.log('resetpassword',req.body)
	if(!req.body.user_email) res.json(404, {error: "Email is required"});
	else
	{
		Users.checking(req.body.user_email).then(function(user) {
		var encodeToken = jwt.encode({user_id:user.attributes.user_id}, secret);
		//var decodeToken = jwt.decode(encodeToken, secret);
		var link  = '94.200.178.58:8000/convo/user/reset/'+encodeToken
		console.log('encodeToken',link);
		//console.log('decodeToken',decodeToken);
			if(user) 
			{
				res.json(user.omit('password,user_salt'));
			}
			else res.json('No Email found.');	
		})
	}
	});
	app.get('/convo/user/reset/:id', function(req, res) {
		//req.params.id;
		var decodeToken = jwt.decode(req.params.id, secret);
		console.log('decodeToken',decodeToken);
		res.json(200);
	});
	
	app.post('/convo/user/update', function(req, res) {
	if(!req.body.user_id || !req.body.token || req.body == undefined) res.json(200, {status: "UserId and Token are required"});
	var data = req.body;
		Users.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			//console.log("data",data)
			delete data.token;
			//console.log("data",data)
			Users.update(data).then(function(user) {
			//console.log("user",user)
				res.json(200);
			});
			
		}
		else res.json(401, {error: "Authentication failed"});
	}).catch(Users.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	
	
	app.post('/convo/user/logout', function(req, res) {
	
	if (!req.body.user_id || !req.body.token) res.json(404, {status: "UserId and token are required"});
		
			UserSession.logoutSession({user_id:req.body.user_id,token:req.body.token}).then(function(notify) {
				utils.redisDelSession(req.body.user_id)
				res.json(200);
				});
	});
	app.post('/convo/user/auth', function(req, res) {
	
	Users.authenticate(req.body.user_id,req.body.token,function(status) {
	if(status) res.json(200,{status:true});
	else res.json(200,{status:false});
	}).catch(Users.NotFoundError, function() {
		res.json(200, {error: 'Could not logout '});
	}).catch(function(err) {
		res.json(200, {error: err.message});
		});
	});

	app.post('/convo/user/changepassword', function(req, res) {
	if (!req.body.user_id || !req.body.token || !req.body.password || !req.body.new_password) res.json(412, {status: "Password and new password all are required."});
	else if (!req.body.confirm_password) res.json(412, {status: "Confirm password is required"});
	else if (req.body.new_password != req.body.confirm_password) res.json(412, {status: "Confirm password and new password does not match."});
	else
	{
		Users.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			Users.changePassword(req.body.user_id,req.body.password,req.body.new_password,function(status) {
			if(status) res.json(202,{status:'Password has been changed successfully.'});
			else res.json(400, {error:'Password does not match.'});
			});
		}
		else res.json(401, {error: "Authentication failed"});
		}).catch(Users.NotFoundError, function() {
			res.json(404, {error: 'Resource not found'});
		}).catch(function(err) {
			res.json(500, {error: err.message});
			});
	}
	});
		
		app.get('/convo/user/session/:id', function(req, res) {
		Users.checking(req.params.id).then(function(user) {
		console.log('user.attributes.user_id',user.attributes.user_id)
		UserSession.checkSessionExist(user.attributes.user_id).then(function(UserSes) {
			console.log('UserSes',UserSes);
		
		});
		
		});
		});
		
		
	app.post('/convo/user/settings', function (req, res) {
	if(!req.body.user_id || !req.body.token || req.body == undefined) res.json(202, {status: "UserId and Token are required"});
	if(!req.body.password) res.json(401, {status: "Require Password"});
	if(debugModeOn) console.log('User Settings : UserCtrl : settings? : ',req.body);
		Users.authenticate(req.body.user_id,req.body.token,function(status) {
		if(status)
		{
			Users.checkPassword(req.body.user_id,req.body.password).then(function(changedPassUser) {
			if(debugModeOn) console.log('User Settings : UserCtrl : checkPassword? : ',changedPassUser);
			if(changedPassUser)
			{	var data = {};
				if(req.body.new_password)
				{
					data = _.omit(req.body,'token','new_password','confirm_password');
					data.password = bcrypt.hashSync(req.body.new_password,changedPassUser.attributes.user_salt);
					
					if(debugModeOn) console.log('User Settings : UserCtrl : after data  : ',data);
					Users.update(data).then(function(user) {
						if(debugModeOn) console.log('User Settings : UserCtrl : after update : ',user);
						res.json(200);
					});
				}
				else
				{
					//console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',req.body.password,changedPassUser.attributes.user_salt)
					data = _.omit(req.body,'token','confirm_password');
					data.password = bcrypt.hashSync(req.body.password,changedPassUser.attributes.user_salt);
					
					if(debugModeOn) console.log('User Settings : UserCtrl : after data not : ',data);
					Users.update(data).then(function(user) {
						if(debugModeOn) console.log('User Settings : UserCtrl : after update not : ',user);
						res.json(200);
					});
				}		
			}
			else 
			{
				res.json(401, {error:'Password does not match'});
			}
			});
			
		}
		else res.json(401, {error: "Authentication failed"});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
		
	};
	