var UserTopic = require('../../server/database/UserTopicShelf.js');
var Users_Sessions = require('../../server/database/UsersShelf.js');
module.exports = function(app) {
	app.post('/convo/User/Topics/Get', function(req, res) {
	if(req.body == undefined ) res.json(412, {error: "Precondition failed"});
	if(!req.body.user_id || !req.body.token) res.json(404, {error: "user_id and Token both are required"});
	Users_Sessions.authenticate(req.body.user_id,req.body.token,function(status) {
	if(status) 
	{
		if(debugModeOn) console.log('/convo/User/Topics/Get : UserTopicCtrl : Return request authentication : '+ status);
		UserTopic.get(req.body.user_id,function(notify){
		if(debugModeOn) console.log('/convo/User/Topics/Get : UserTopicCtrl : Return request UserTopic : '+ notify);
		if (typeof notify !== 'undefined' && notify.length > 0 && notify != "") res.json(200,notify);
		else res.json(200, {});
		});
	}
	else res.json(401, {error: "Authentication failed"});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});	
	});
	app.post('/convo/User/Topic/Update', function(req, res) {
	if(req.body == undefined ) res.json(412, {error: "Precondition failed"});
	if(!req.body.user_id || !req.body.token) res.json(404, {error: "user_id and token both are required"});
	if(!req.body.data) res.json(404, {error: "Topics data must be an array with mentioned key value formats"});
	
	Users_Sessions.authenticate(req.body.user_id,req.body.token,function(status) {
	if(status) 
	{
		UserTopic.update(req.body.user_id,req.body.data,function(notify){
		if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,{status:'Updated'});
		else res.json(501, {error:'No Topic is selected for Adding'});
		});
	}
	else res.json(401, {error: "Authentication failed"});
	}).catch(UserTopic.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
		
	
	
	app.post('/convo/User/Topic/Delete', function(req, res) {
	if(req.body == undefined ) res.json(412, {error: "Precondition failed"});
	if(!req.body.user_id || !req.body.token) res.json(404, {error: "user_id and token both are required"});
	if(!req.body.data) res.json(404, {error: "Topics data must be an array with mentioned key value formats"});
	
	Users_Sessions.authenticate(req.body.user_id,req.body.token,function(status) {
	if(status) 
	{
		UserTopic.deleted(req.body.user_id,req.body.data,function(notify){
		if (typeof notify !== 'undefined' && notify.length > 0) res.json(200,{status:'Deleted'});
		else res.json(501, {error:'No result'});
		});
	}
	else res.json(401, {error: "Authentication failed"});
	}).catch(UserTopic.NotFoundError, function() {
		res.json(404, {error: 'Resource not found'});
	}).catch(function(err) {
		res.json(500, {error: err.message});
		});
	});
	
	
};
	