var utils = require('../../server/controllers/utils.js');
module.exports = function(app) {
  app.post('/rooms', utils.restrict, function(req, res) {
    utils.getPublicRoomsInfo(client, function(rooms) {
      res.render('room_list', { rooms: rooms });
    });
  });

  /*
   * Create a rooom
   */

  app.post('/convo/topic/conversation/create', utils.restrict, function(req, res) {
  //	console.log(req.body);
    utils.validRoomName(req, res, function(roomKey) {
      utils.roomExists(req, res, function() {
        utils.createRoom(req, res);
      });
    });
  });

  /*
   * Join a room
   */

  app.get('/convo/topic/conversation/:id', utils.restrict, function(req, res) {
    utils.getRoomInfo(req, res, client, function(room) {
      utils.getUsersInRoom(req, res, client, room, function(users) {
        utils.getPublicRoomsInfo(client, function(rooms) {
          utils.getUserStatus(req.user, client, function(status) {
            utils.enterRoom(req, res, room, users, rooms, status);
          });
        });
      });
    });
  });



	
};
	