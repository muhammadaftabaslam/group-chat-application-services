var usershelf = require('../../server/database/UsersShelf.js');
var Connection = require('../../server/database/ConnectionsShelf.js');
var utils = require('../../server/controllers/utils.js');
var io = require('socket.io')(3000);
sub = redis_store.createClient();
pub = redis_store.createClient();
io.sockets.setMaxListeners(0);
sub.subscribe('public');
//io.set('transports', ['websocket','flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling', 'polling']);
 //io.use('transports',['websocket']);
io.set('authorization', function (handshakeData, callback) {
	console.log('handshakeData request get',handshakeData._query.user_id,handshakeData._query.token)
	if(!handshakeData._query.user_id || !handshakeData._query.token) callback("user_id and Valid token must be require for the socket connection", false);
	else
	{
		redis.hget("Profile:"+handshakeData._query.user_id,handshakeData._query.token,function(erro,value){
			if(value) 
			{
				usershelf.authenticate(handshakeData._query.user_id,handshakeData._query.token,function(status) {
				console.log("socket connected");
				if(status) 
					callback(null, true);
				else callback(null, false);
				}).catch(usershelf.NotFoundError, function() {
					callback("User Not found", false);
				}).catch(function(err) {
					callback( err.message, false);
					});
			}
		else callback("User not found", false);
		});
	}	
	//callback(null, true);
});

io.on('connection', function (socket) {
	socket.join('public');
	//console.log("hello this is connected",socket.handshake.query.user_id+"               "+socket.id);

	redis.hset('AllSockets',socket.handshake.query.user_id, socket.id);
	redis.hget('AllSockets',socket.handshake.query.user_id,function(er,value){
	//console.log("socket in redis",value);
	});
  //console.log('socket',socket.nsp.server.eio.transports);
  socket.on('news', function (data) {
    //console.log(data);
	socket.emit('response', { hello: 'world' });
  });
  
  socket.on('Create Room', function (data) {
    console.log('Create Room',data);
	utils.validRoomName(data, function(err,ok) {
	
    if(!err && ok)  utils.roomExists(data, function(err1,ok1) {
        if(!err1 && ok1) utils.createRoom(data,function(err2,obj){
		if(!err2 && obj) 
		Connection.getconnectionsFollower(data.user_id,function(notify) {
		
		obj.Followers = notify;
		console.log('Create Room notify',obj);
			socket.emit('Room Created',obj );
			//pub.publish('public', obj);
			socket.join(obj.Roomkey);
			});
		
		// socket.emit('Room Created',obj);
		// socket.join(obj.Roomkey);
		});
      });
    });
	
  });
   socket.on('Room User Invite', function (data) {
	redis.hgetall('AllSockets',function(errforhash,hashvalue){
		if(!errforhash && hashvalue)
		{
			data.InvitedUsers.forEach(function(val, index) {
				if(hashvalue[val])
				{	console.log('yahooooo       id : '+val + '  SocketId : '+hashvalue[val]);
					socket.broadcast.to(hashvalue[val]).emit('Room Notification', {Message:data.Message,Roomkey:data.Roomkey});
				}
				else
				{
					console.log('user is not online id: '+val + '  SocketId : '+hashvalue[val])
				}
				
			});
		}
	});
    console.log('pending task',data);
	socket.emit('Room User Invited',true);
	
  });
  
	socket.on('User Followers', function (data) {
    Connection.getconnectionsFollower(data.user_id,function(followers) {
				socket.emit('User Followers Data',followers);
			});
	
  });
	socket.on('User Followings', function (data) {
    Connection.getconnectionsFollowings(data.user_id,function(followers) {
				socket.emit('User Followings Data',followers);
			});
  });
  
    socket.on('Room Chat', function (data) {
    console.log('User Invite',data);
	socket.emit('Room Chat Reply',true);
  });
   socket.on('Leave Room', function (data) {
    console.log('Leave Room',data);
	
	redis.hdel('rooms:' + data.Roomkey + ':online', data.user_id);
		redis.zadd("Online",1,data.user_id,function(errofzadd,dataofzadd){
			
						redis.hincrby('rooms:' + data.Roomkey + ':info', 'OnlineUsers', -1);
		
			});
	socket.leave(data.Roomkey);

  });
  socket.on('New User Join', function (data) {
	socket.username = data.username;
	socket.room = data.Roomkey;
    console.log('New User Join on on',data);
	utils.newUserJoinRoom(data, function(ok,status) {
	if(ok && status)
	{
		socket.join(data.Roomkey);
		//console.log('status',ok);
		socket.emit('New User Joined',status);
		io.sockets.in(data.Roomkey).emit('New User', {
				Roomkey : data.Roomkey,
                username: data.username,
                Status: status || 'available'
              });
		
	}
    else	socket.emit('Error',{Message:"Room already Joined by this User"});
    });
	
  });
  
   socket.on('All Conversations', function () {
   // console.log('All Conversations');
	utils.getPublicRoomsInfo(function(status) {
	//console.log('getPublicRoomsInfo',status);
	//if(status)
	//{
		socket.emit('Return Conversations',status);
	//}
    
    });
	
  });
  
  
  
  
  
  
  
  
   // sub.on('message', function (channel, message) {
	// console.log("redis sub message",channel,message);
        // socket.emit(channel, message);
    // });
  
  
  
  
  
  
  
  
  
  
   socket.on('Message', function (data) {
   
    console.log('New message',data);
	data.DateTime = new Date();
	redis.hincrby('rooms:' + data.Roomkey + ':info', 'Messages', 1,function(err,NoOfMsgs){
				data.MsgId = NoOfMsgs;
			redis.hset('rooms:' + data.Roomkey + ':info', NoOfMsgs,  JSON.stringify(data));
			});
	io.to(data.Roomkey).emit("Message Reply",data);
	//var check = findClientsSocketByRoomId( data.Roomkey)
	//console.log('code from stack overflow: ',check,check.length)
	//socket.broadcast.to(socket.room).emit('Message Reply',data);
  });
  
  
  function findClientsSocketByRoomId(roomId) {
var res = []
, room = io.sockets.adapter.rooms[roomId];
if (room) {
    for (var id in room) {
    res.push(io.sockets.adapter.nsp.connected[id]);
    }
}
return res;
}

  
  
  
});

