var crypto = require('crypto')
  , type = require('component-type')
  ,	usershelf = require('../../server/database/UsersShelf.js')
  , ConversationRoomShelf = require('../../server/database/ConversationRoomShelf.js')
  , _ = require('underscore');
/*
 * Restrict paths
 
 //  1 for public
	 2 for private
 */

exports.restrict = function(req, res, next){
next();
  // if(req.isAuthenticated()) next();
  // else res.redirect('/');
};
exports.UserCurrentConversation = function(user_id,fn) { 
	redis.hget("Profile:"+user_id,"room_id",function(err, joinedRoom) {
	if(joinedRoom)
	{
		
		exports.userConversation(joinedRoom,function(res){
		console.log('joinRoomInRedis',res);
			fn(res);
		})
	}
	else
	{
		fn(false);
	}
	});

}
/*
 * ConversationLink
 */    
 exports.userConversation = function(conversation_room_id,fn) { 
	var response = {};
	response.Messages = [];
	redis.hgetall('rooms:' + conversation_room_id + ':info', function(err, Conversation) {
	if(!err && Conversation) 
	{
		response.Active = true;
	response.Conversation = Conversation;
	redis.lrange('Room:Messages:' + conversation_room_id,-10,-1,function(msgerror,lastmessages){
		redis.lrange('Room:Admins:' + conversation_room_id,0,1,function(adminerror,admins){
			parseArray(lastmessages,function(messages){
				parseArray(admins,function(parsedAdmins){
					response.Admins = parsedAdmins;
					response.Messages = messages;
					response.TotalMessages = messages.length;
					response.TotalOnlineUsers = parsedAdmins.length;
					//console.log('joinRoomInRedis',response);
						fn(response);
					
				})
							
			});
		
		});
	});
	}
	});		
		
};
exports.ConversationLink = function(conversation_room_id,fn) { 
	var response = {};
	response.Messages = [];
	redis.hgetall('rooms:' + conversation_room_id + ':info', function(err, Conversation) {
	if(!err && Conversation) 
	{
		response.Active = true;
	response.Conversation = Conversation;
	redis.lrange('Room:Messages:' + conversation_room_id,-10,-1,function(msgerror,lastmessages){
		redis.lrange('Room:Admins:' + conversation_room_id,0,1,function(adminerror,admins){
			parseArray(lastmessages,function(messages){
				parseArray(admins,function(parsedAdmins){
					response.Admins = parsedAdmins;
					response.Messages = messages;
					response.TotalMessages = messages.length;
					response.TotalOnlineUsers = parsedAdmins.length;
					//console.log('joinRoomInRedis',response);
						fn(response);
					
				})
							
			});
		
		});
	});
	}
	else
	{
		//conversation is closed
		ConversationRoomShelf.ClosedConversation(conversation_room_id).then(function(Conversation){
		if(Conversation)
		{
			response.Active = false;
			response.Conversation = Conversation;
			fn(response);
		}
		else
		{
			fn({Error:'No conversation found.'});
		}
		})
		
	}		
	});		
		
};
var parseArray = function(obj,fn) {
var res = [];
obj.forEach(function(key) {
    res.push(JSON.parse(key));
});
	
	fn(res);
};
/*
 * Generates a URI Like key for a room
 */    
exports.changeTopicIdToName = function(data,fn) { 
redis.hgetall('Topics', function(err, room) {
				if(!err && room)
				{
					data.forEach(function (key)
					{
						key.topic_name = room[key.topic_id]
					});
					//console.log(data)
					fn(data)
				}
				else 
				{
					fn()
				}
				
				
				});


};

exports.mergeDuplicateFeatureData = function(data,fn) { 
var tmp = {}
var obj = {}
		data.forEach(function (key)
			{
				obj[key.conversation_room_id]	 = {}
				obj[key.conversation_room_id].conversation_room_name	 = key.conversation_room_name
				obj[key.conversation_room_id]['topic_id']	 = key.topic_id
				obj[key.conversation_room_id]['topic_name']	 = key.topic_name
				obj[key.conversation_room_id].user = []
				//_.omit(key,'conversation_room_name','topic_id','topic_name')
			});
			data.forEach(function (key)
			{
				var obj1 = {}
				obj1.user_id = key.user
				obj1.user_avatar = key.user_avatar
				obj1.username = key.username
				obj[key.conversation_room_id].user.push(obj1)
				
			});
	//console.log('obj',obj)
	fn(obj)
	//console.log('data',data)
};


 
exports.genRoomKey = function() {
  var shasum = crypto.createHash('sha1');
  shasum.update(Date.now().toString());
  return shasum.digest('hex').substr(0,6);
};

exports.redisSession = function(user_id,token,email) {
			redis.hset("Profile:"+user_id,token,"token");
			redis.hincrby("Profile:"+user_id,'LoggedIn',1);
			if(email)redis.hset("Profile:"+user_id,"email",email);
			redis.zadd("Online",1,user_id);
			redis.set("Profile1:"+user_id,10,"EX",3600)
};

exports.redisDelSession = function(user_id,token) {

	redis.hget("Profile:"+user_id,'LoggedIn',function(error,value){
		if(value == 1)
		{
			redis.del("Profile:"+user_id);
			redis.del("UserNotification:"+user_id);
			redis.del("Profile1:"+user_id);
		}
		else
		{
			redis.zrem("Online",user_id);
			redis.hincrby("Profile:"+user_id,'LoggedIn',-1);
			redis.hdel("Profile:"+user_id,token,function(err, userAdded) {});
			//callback(value);
		}
	
	});
};
/*
 * Room name is valid
 */
