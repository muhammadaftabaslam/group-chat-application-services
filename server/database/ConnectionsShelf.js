	var Connection = bookshelf.Model.extend({
	  tableName: 'user_connections',
	  idAttribute: 'user_connection_id',
		initialize: function () {
		this.on('saving', this.validateSave);

	  },
		account: function() {
			return this.belongsTo(Account);
		},

	}, 
	{
		addConnection: Promise.method(function(obj,callback) {
			new Connections(obj).invokeThen('save', null,null).then(function(response) {
						callback(response);
					});
		}),
		removeConnection: Promise.method(function(obj,callback) {
			var query = bookshelf.knex("user_connections").del().where(obj);
			query.exec( function(err){
			if(err)
			{
				console.log('removeConnection : UserCtrl : Request get : ', err);
				return callback(err);
			}
			else callback();
			})
		}),
		update: Promise.method(function(obj,callback) {
		return UserNotification.query({where: {user_id: obj.user_id,user_connection_id: obj.user_connection_id}}).fetchAll().then(function(model) {
		if (model) 
		{
				//redis
		}
		});
	
		}),
		getconnectionsFollowings: Promise.method(function(user_id,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
			
			bookshelf.knex('users').join('user_connections', 'user_connections.connection_user_id', '=', 'users.user_id').where('user_connections.user_id',user_id).select('users.user_id','users.username','user_avatar','user_first_name','user_last_name').
			then(function(response) 
			{
				//console.log('getconnectionsFollowings in connection shelf',response,user_id);
				callback(response);
			});
		}),
		getconnectionsFollower: Promise.method(function(user_id,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
			bookshelf.knex('users').join('user_connections', 'user_connections.user_id', '=', 'users.user_id').where('user_connections.connection_user_id',user_id).select('users.user_id','users.username','user_avatar','user_first_name','user_last_name').
			then(function(response) 
			{
				//console.log('getconnectionsFollower in connection shelf',response,user_id);
				callback(response);
			});	
				
		}),
		checkConnection: Promise.method(function(obj,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
			var query = bookshelf.knex("user_connections").where(obj);
			query.exec( function(err,model){
			if(!err && model.length > 1)
			{
				console.log('check : connectionshelf : query result : ',model);
				return callback(true);
			}
			else 
			{	
				console.log('check : connectionshelf : query result : ', err);
				callback(false);
			}
			})
				
		})
			
	
});
var Connections = bookshelf.Collection.extend({
			model: Connection
		});
	module.exports = Connection;