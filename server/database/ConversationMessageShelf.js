
		var ConversationMessage  = bookshelf.Model.extend({
		  tableName: 'Topic',
		  idAttribute: 'TopicId',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		}, 
		{
			insert:Promise.method(function(obj) {
		
			return new ConversationMessage(obj).save().then(function(model) {
				if(model)  return model;
				else return;
			});
		
		})
		
		});
		var ConversationMessages = bookshelf.Collection.extend({
			model: ConversationMessage
		});
		
		
		
		
	module.exports = ConversationMessage;