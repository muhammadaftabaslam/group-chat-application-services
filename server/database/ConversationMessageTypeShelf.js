

		var ConversationMessageType  = bookshelf.Model.extend({
		  tableName: 'Topic',
		  idAttribute: 'TopicId',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		}, 
		{});
		var ConversationMessageTypes = bookshelf.Collection.extend({
			model: ConversationMessageType
		});
		
		
		
		
	module.exports = ConversationMessageType;