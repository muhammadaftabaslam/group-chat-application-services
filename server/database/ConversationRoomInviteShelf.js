

		var ConversationRoomInvite = bookshelf.Model.extend({
		  tableName: 'ConversationRoomInvite',
		  idAttribute: 'conversation_room_invite_id',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		}, 
		{
			insert:Promise.method(function(obj,callback) {
			
				return new ConversationRoom(obj).save().then(function(model) {
					if(model)  return model;
					else return;
				});
			
			})
		});
		var ConversationRoomInvites = bookshelf.Collection.extend({
			model: ConversationRoomInvite
		});
		
		
		
		
	module.exports = ConversationRoomInvite;