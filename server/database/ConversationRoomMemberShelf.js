
		var ConversationRoomMember = bookshelf.Model.extend({
		  tableName: 'conversation_room_members',
		  idAttribute: 'conversation_room_member_id',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		}, 
		{
			insert:Promise.method(function(obj,callback) {
				ConversationRoomMember.query({where: obj}).fetchAll().then(function(model) {
				
				if (model.length == 0) 
				{
					obj.conversation_room_member_joining_date = new Date();
					new ConversationRoomMember(obj).save().then(function(model) {
						callback(true);
					});
				}
				else
				{
					callback(false);
				}
				});
			
			
		
			}),
			conversationUsers:Promise.method(function(id,callback) {
		
				 ConversationRoomMember.query({where: {conversation_room_id:id}}).fetchAll().then(function(model) {
						 callback(model);
			 });	
		
			})
		
		
		});
		var ConversationRoomMembers = bookshelf.Collection.extend({
			model: ConversationRoomMember
		});
		
		
		
		
	module.exports = ConversationRoomMember;