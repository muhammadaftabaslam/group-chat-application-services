
		var ConversationRoom = bookshelf.Model.extend({
		  tableName: 'conversation_rooms',
		  idAttribute: 'conversation_room_id',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		},
		{
		update: Promise.method(function(obj,fn) {
			ConversationRoom.forge({conversation_room_id:obj.conversation_room_id}).fetch().then(function(model) {
			//console.log('model',model);
				if (model) 
				{ 
					model.set(obj).save().then(function(model1) {
					//console.log('model1',model1);
						fn(model1);
					})
				}
				else fn(false);
			});
		}),
		insert:Promise.method(function(obj,callback) {
		
			return new ConversationRoom(obj).save().then(function(model) {
				if(model)  return model;
				else return;
			});
		
		}),
		incrementMsgCount:Promise.method(function(id) {
			var obj = 
			{
				conversation_room_id:id
			};
			return ConversationRoom.forge({conversation_room_id:id}).fetch().then(function(model) {
			if (model) 
			{
				
				obj.conversation_room_message_count = model.attributes.conversation_room_message_count +1;
				model.set(obj).save().then(function(model1) {
					return model1;
				})
			}
			else return;
			});
		
		}),
		incrementMaxUsers:Promise.method(function(id) {
			var obj = 
			{
				conversation_room_id:id
			};
			return ConversationRoom.forge({conversation_room_id:id}).fetch().then(function(model) {
			if (model) 
			{
				obj.conversation_room_max_users = model.attributes.conversation_room_max_users +1;
				model.set(obj).save().then(function(model1) {
					return model1;
				})
			}
			else return;
			});
		
		}),
		closeRoom:Promise.method(function(id) {
			var obj = 
			{
				conversation_room_id:id
			};
			return ConversationRoom.forge({conversation_room_id:id}).fetch().then(function(model) {
			if (model) 
			{
				obj.conversation_room_is_active = 0;
				model.set(obj).save().then(function(model1) {
					return model1;
				})
			}
			else return;
			});
		
		}),
		search:Promise.method(function(str) {
		
			return ConversationRooms.query(function(qb) {
			qb.where('conversation_room_name', 'LIKE', '%'+str+'%')}).fetch().then(function(model) {
				if(model) return model
				else return
			
			})
		
		}),
		conversationsCount:Promise.method(function(str) {
			
			return ConversationRooms.query(function(qb) {
			qb.where('conversation_room_name', 'LIKE', '%'+str+'%')}).fetch().then(function(model) {
				if(model) return model.length
				else return
			
			})
		
		}),
		conversationWrtTopicName:Promise.method(function(str) {
			
			return ConversationRooms.query(function(qb) {
			qb.where('conversation_room_name', 'LIKE', '%'+str+'%')}).fetch().then(function(model) {
			//console.log(' conversationWrtTopicName model',model)
				if(model) return model
				else return
			
			})
		
		}),
		createConversation:Promise.method(function(str) {
			
			return ConversationRooms.query(function(qb) {
			qb.where('conversation_room_name', 'LIKE', '%'+str+'%')}).fetch().then(function(model) {
			//console.log(' conversationWrtTopicName model',model)
				if(model) return model
				else return
			
			})
		
		}),
		updateConversation:Promise.method(function(str) {
			
			return ConversationRooms.query(function(qb) {
			qb.where('conversation_room_name', 'LIKE', '%'+str+'%')}).fetch().then(function(model) {
			//console.log(' conversationWrtTopicName model',model)
				if(model) return model
				else return
			})
		}),
		userConversation:Promise.method(function(id) {
			
			return ConversationRooms.query(function(qb) {
			qb.where('conversation_room_created_by', '=', id)}).fetch().then(function(model) {
			//console.log(' conversationWrtTopicName model',model)
				if(model) return model
				else return
			
			})
		
		}),
		getConversation:Promise.method(function(id) {
			
			return ConversationRoom.forge({conversation_room_id:id}).fetch().then(function(model) {
				if (model) return model;
				else return;
			})
		}),
		ClosedConversation:Promise.method(function(id) {
			return bookshelf.knex('conversation_rooms').join('users', 'conversation_rooms.conversation_room_created_by', '=', 'users.user_id').where('conversation_rooms.conversation_room_id',id).select('conversation_rooms.*','users.user_id','users.username','user_avatar','user_first_name','user_last_name').
			then(function(response) 
			{
				//console.log('getconnectionsFollower in connection shelf',response,user_id);
				return response;
			});	
			
		})
		
		
		});
		var ConversationRooms = bookshelf.Collection.extend({
			model: ConversationRoom
		});
		
		
		
		
	module.exports = ConversationRoom;