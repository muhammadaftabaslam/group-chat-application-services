
		var ConversationRoomType = bookshelf.Model.extend({
		  tableName: 'ConversationRoomType',
		  idAttribute: 'TopicId',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		}, 
		{});
		var ConversationRoomTypes = bookshelf.Collection.extend({
			model: ConversationRoomType
		});
		
		
		
		
	module.exports = ConversationRoomType;