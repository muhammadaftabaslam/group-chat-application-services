var dbConfig = require('../../config/database.js');
var mysql      = require('mysql');
var connection = mysql.createConnection(dbConfig.connection);
connection.connect();
exports.featuresConversation = function(callback) {


		connection.query('Select crm.conversation_room_id,conversation_room_name,topic_id, crm.user_id user,username,user_avatar'+ 
		' from conversation_rooms  cr join conversation_room_members crm on cr.conversation_room_id=crm.conversation_room_id '+
		'join (select  conversation_room_id,Count(1) num from conversation_room_members '+
		'group by conversation_room_id order by num desc limit 3)temp  on temp.conversation_room_id=crm.conversation_room_id '+
		'join users u on u.user_id = crm.user_id order by num desc',
		function(err, rows, fields) {
		if (!err)
		{
			//console.log('The solution is: featuresConversation ', rows);
			callback(rows)
		}
		else
		{		  
			console.log('Error while performing Query featuresConversation.',err);
			callback(false)
		}
		});

	};
exports.topTopics = function(callback) {


	connection.query('Select distinct topic_id from conversation_rooms  cr join (select  conversation_room_id,Count(1) num from conversation_room_members group by conversation_room_id order by num desc) temp  on temp.conversation_room_id=cr.conversation_room_id limit 3',
	function(err, rows, fields) {
	if (!err)
	{
		//console.log('The solution is topTopics: ', rows);
		callback(rows)
	}
	else 
	{
	  console.log('Error while performing Query. topTopics',err);
	  callback(false)
	}
	});
	}
	
	exports.convoByTopics = function(str,callback) {
	connection.query("Select t.topic_id,t.topic_name,  count(conv.conversation_room_id) as ConvoCount "+
					"from conversation_rooms conv right JOIN "+
					"topics t ON t.topic_id = conv.topic_id "+
					" where t.topic_name like "+"'%"+str+"%'"+
					" group by t.topic_id,t.topic_name "+
					"order by ConvoCount desc",
	function(err, rows, fields) {
	if (!err)
	{
		//console.log('The solution is topTopics: ', rows);
		callback(rows)
	}
	else 
	{
	  console.log('Error while performing Query. topTopics',err);
	  callback(false)
	}
	});
}
exports.friends = Promise.method(function(id,callback){
	connection.query("SELECT distinct users.user_id,users.user_first_name,users.user_last_name,users.username,users.user_email,users.user_avatar FROM users LEFT JOIN user_connections ON  users.user_id = user_connections.connection_user_id "+
	"WHERE (user_connections.user_id <>"+id+" && user_connections.connection_user_id <> "+id+" && users.user_id <> "+id+") OR (user_connections.connection_user_id is null && users.user_id <> "+id+")",
	function(err, rows, fields) {
	if (!err)
	{
		if(debugModeOn) console.log('friendslist : FeatureShelf : Freinds are: ', rows);
		callback(rows)
	}
	else 
	{
	  console.log('Error while performing Query. topTopics',err);
	  callback(false)
	}
	});
});
exports.userInvolveInConversation = Promise.method(function(id,callback){
	connection.query("SELECT distinct conversation_rooms.conversation_room_name FROM conversation_rooms "+
"LEFT JOIN conversation_room_members ON  conversation_rooms.conversation_room_id = conversation_room_members.conversation_room_id "+
"WHERE conversation_room_members.user_id = "+id,
	function(err, rows, fields) {
	if (!err)
	{
		//if(debugModeOn) console.log('userInvolveInConversation : FeatureShelf : Freinds are: ', rows);
		callback(rows);
	}
	else 
	{
	  console.log('Error while performing Query. topTopics',err);
	 callback(false);
	}
	});
});
exports.landingPageConversation = Promise.method(function(callback){
	connection.query("select c.conversation_room_id ,c.conversation_room_name ,c.conversation_room_type_id,c.conversation_room_creation_date,c.conversation_room_topic,c.conversation_room_max_users "+
					",c.conversation_room_avatar,c.topic_id "+
					"from conversation_rooms c "+
					"left join conversation_room_members m on c.conversation_room_id=m.conversation_room_id where c.conversation_room_is_active = 1 "+
					"group by c.conversation_room_id ,c.conversation_room_name ,c.conversation_room_creation_date,c.conversation_room_max_users "+
					",c.conversation_room_avatar,c.topic_id",
	function(err, rows, fields) {
	if (!err)
	{
		//if(debugModeOn) console.log('userInvolveInConversation : FeatureShelf : Freinds are: ', rows);
		callback(rows);
	}
	else 
	{
	  console.log('Error while performing Query. topTopics',err);
	 callback(false);
	}
	});
});
exports.landingPageUsersInConversation = Promise.method(function(callback){
	connection.query("select crm.conversation_room_id, crm.user_id,  u.user_avatar,u.username from `conversation_room_members` crm inner join users u ON crm.user_id = u.user_id "+
	"group by crm.conversation_room_id, crm.user_id",
	function(err, rows, fields) {
	if (!err)
	{
		//if(debugModeOn) console.log('userInvolveInConversation : FeatureShelf : Freinds are: ', rows);
		callback(rows);
	}
	else 
	{
	  console.log('Error while performing Query. topTopics',err);
	 callback(false);
	}
	});
});
// exports.featuresConversation = function(callback) {
	// bookshelf.knex('conversation_rooms')
	// .join('conversation_room_members', 'conversation_rooms.conversation_room_id', '=', 'conversation_room_members.conversation_room_id')
	//.where('conversation_rooms.conversation_room_creation_date','>','2015-01-01')
	// .select('conversation_rooms.conversation_room_name','conversation_rooms.topic_id','conversation_room_members.user_id')
	// .then(function(response) 
	// {
		// callback(response);
	// });		
// };


