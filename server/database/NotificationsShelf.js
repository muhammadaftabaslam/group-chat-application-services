var _ = require('underscore')
var Users_Sessions = require('../../server/database/UsersShelf.js');

	var Notifications = bookshelf.Model.extend({
	  tableName: 'notifications',
	  idAttribute: 'notification_id',
		initialize: function() {
			this.on('saving', this.validateSave);
		},
		account: function() {
			return this.belongsTo(Account);
		},

	}, {
	get: Promise.method(function(callback) {
		Notifications.query('where', 'user_is_email_notification', '=', 1).fetchAll().then(function(model) {	
				if(model) callback(model);
				else callback();
		});
	  }),
	  
	  insert: Promise.method(function(callback) {
		Notifications.query('where', 'user_is_email_notification', '=', 1).fetchAll().then(function(model) {	
				callback(model);
		});
	  })
	
		
	
});
module.exports = Notifications;