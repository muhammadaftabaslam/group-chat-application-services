	var UserSession = bookshelf.Model.extend({
	  tableName: 'user_sessions',
	  idAttribute: 'session_id',
		initialize: function () {
		this.on('saving', this.validateSave);

	  },
		account: function() {
			return this.belongsTo(Account);
		},

	}, 
	{
	  addSession: Promise.method(function(obj) {
	 
		obj.session_date = new Date();
		 console.log("addsession",obj)
		return new UserSession(obj).save().then(function(session) {
			//redis.hset("Profile:"+obj.user_id,"SessionDate",session.session_date,function(err,status){
			return session;
			});
	
		}),
		getSession: Promise.method(function(user_id,token) {
			return new this({user_id:user_id,token:token}).fetch({require: true}).tap(function(session) {
				return session;
			});
	
		}),
		checkSession: Promise.method(function(user_id,token) {
			return redis.exists("Profile:"+user_id,function(err,status){
			if(!err)
			{
			console.log("redis1",status);
				if(status)	
				{
				return UserSession.getSession(user_id,token).then(function(session){
					if(session)
						{
							if(session.attributes.logout_timestamp)	return false; 
							
							else return true;
							
							
						}
						else
						{
							return false;
						}
					});
				}
				else 
				{
					
					return false;
				}
			}
			
			
			});
	
		}),
		logoutSession: Promise.method(function(obj) {
			return UserSession.forge({token:obj.Token}).fetch().then(function(model) {
						if (model) {
						obj.logout_timestamp = new Date();
							model.set(obj).save().then(function(model) {
							redis.del("Profile:"+obj.user_id);	
							return model;
							}).otherwise(function(err) {
								throw new Error(err.message);
							});
						} else return true;
					});
	  }),
	  checkSessionExist: Promise.method(function(id) {
			return UserSession.forge({user_id:id,logout_timestamp:null}).fetch().then(function(model) {
						if (model) {
							return model
						} else return false;
					});
	  })
	
	
});
module.exports = UserSession;