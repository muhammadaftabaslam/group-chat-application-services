var bcrypt   = Promise.promisifyAll(require('bcrypt'));
var jwt = require('jwt-simple');
var path = require('path');
var secret = 'xxx';
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var fs = require('fs');
var salt = "";
var emailNotify = require('../../server/notification/emailNotification.js');


	
	var Social = bookshelf.Model.extend({
	  tableName: 'user_social',
	  idAttribute: 'Id',
		initialize: function() {
			this.on('saving', this.validateSave);
		},
		account: function() {
			return this.belongsTo(Account);
		},

	}, {
		
		check: Promise.method(function(id) {
		if(id)
		{
			//console.log('id in social shelf',id)
			return Social.forge({social_id:id}).fetch().then(function(user) {
			//console.log('user id in social shelf')
			if(user) return user;
			else return false;
			
			});
			}
			else{
				console.log('socialshelf:checking for user in social table : id is not defined')
				return false;
			}

		
	  }),
	  changePassword: Promise.method(function(Userid,oldPassword,newPassword,callback) {
		
		return Users.forge({UserId:Userid}).fetch().then(function(model) {
						if (model) 
						{
							//console.log(model);
							var hash = bcrypt.hashSync(oldPassword,model.get('Salt'));
							if(model.get('Password') == hash)
							{
								model.set({Password:bcrypt.hashSync(newPassword,model.get('Salt'))}).save().then(function(model1) {
								callback(model1);
								}).otherwise(function(err) {
									throw new Error(err.message);
								});
							}
							else throw new Error("Wrong Password");
						} else throw new Error("User not found");
					});
	  }),
	  	register: Promise.method(function(obj) {
		
			return new Social(obj).save().then(function(model) {
			return model;
		});
	  }),
		getUser: Promise.method(function(id,callback) {
			new Users({'UserId': id}).fetch().then(function(users) {
			callback(users);
		})
			
		}),
		check1: Promise.method(function(obj) {
		//if (Email || Password) throw new Error('Email and Password are both required');
		return new this(obj).fetch({require: true}).tap(function(user) {
		return user;
		});
	  }),
		update: Promise.method(function(obj) {
		if (!obj.UserId) throw new Error('Please provide UserId.');
			return Users.forge({UserId:obj.UserId})
					.fetch()
					.then(function(model) {
						if (model) { 
							model.set(obj).save().then(function(model) {
								
							}).otherwise(function(err) {
								throw new Error(err.message);
							});
						} else throw new Error("User not found");
					});
		}),
	
});
	
	
	
	module.exports = Social;
