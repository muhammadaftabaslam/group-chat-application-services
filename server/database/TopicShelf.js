
		var Topic  = bookshelf.Model.extend({
		  tableName: 'topics',
		  idAttribute: 'topic_id',
			initialize: function() {
				this.on('saving', this.validateSave);
			},
			account: function() {
				return this.belongsTo(Account);
			},

		}, 
		{
		get:Promise.method(function() {
		var rooms = {}
			return new Topic().fetchAll().then(function(Topicc) {
			var tempdata = Topicc.toJSON()
			tempdata.forEach(function (key)
			{
				rooms[key.topic_id] = key.topic_name
			});
			redis.hmset('Topics', rooms)
				return Topicc
		  
			})
			
		
		}),
		search:Promise.method(function(str) {
			return Topics.query(function(qb) {
			qb.where('topic_name', 'LIKE', '%'+str+'%')}).fetch().then(function(model) {
				if(model) return model
				else return
			
			})
			
		
		})
		
		
		
		});
		
		var Topics = bookshelf.Collection.extend({
			model: Topic
		});
		
	
	
module.exports = Topic;

	