	var Notifications = require('../../server/database/NotificationsShelf.js');
	var _ = require('underscore');
	var UserNotification = bookshelf.Model.extend({
	  tableName: 'user_notifications',
	  idAttribute: 'user_notification_Id',
		initialize: function() {
			this.on('saving', this.validateSave);
		},
		account: function() {
			return this.belongsTo(Account);
		},
	

	}, {
		get: Promise.method(function(obj,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
		//console.log("get request",obj);
		return UserNotification.query({where: {user_id: obj.user_id,Active: 1}}).fetchAll().then(function(model) {
		if (model) { 
			
			var data = model.toJSON();
			//console.log("model",data);
			Notifications.get(function(notify)
			{ 
				var noti = notify.toJSON();
			//console.log("notify");
				if(notify && data.length > 0)
				{
					//console.log("inside",notify);
					
					//console.log("inside",noti);
					var inc = 0;
					for (var p in data) {
						//console.log("p is "+p);
							for (var s in noti) {
								//console.log("s is "+s);
								if(data[p].notification_id == noti[s].notification_id)
								{
									noti[s].Active = true;
								}
								noti[s] = _.omit(noti[s],'user_notification_url','user_is_email_notification');
							}
							inc++;
							if(inc == data.length)
							{				
								//console.log("noti",noti);
								callback(noti);
							}
							//console.log("noti",noti);
							
							
						}
				}
				else callback(noti);
			});
			
			} 
			else callback();
					});
	  }),	  
	  insert: Promise.method(function(user_id,obj,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
		//if(debugModeOn) console.log('insert : UserNotificationShelf : Request get : ',user_id,obj);
		var realdata = [];
		for (var s in obj) {
			
					obj[s].user_id = user_id;
					obj[s].Active = 1;
					realdata.push(obj[s]);
			
			}
			//console.log("realdata : ",realdata);
			var usernoti = UserNotification.forge(realdata);
			UserNotification.query({where: {user_id:user_id}}).fetchAll().then(function(model) {
			
				model.invokeThen('destroy').then(function() {
				
				
				new UserNotifications(realdata).invokeThen('save', null,null).then(function(response) {
				//console.log('response',response);
						callback(response);
					});
						
					});
				  
				});					
	  })
	
});
var UserNotifications = bookshelf.Collection.extend({
			model: UserNotification
		});
		
		module.exports = UserNotification;