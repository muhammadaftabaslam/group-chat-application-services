var _ = require('underscore')

		var UserTopic  = bookshelf.Model.extend({
		  tableName: 'user_topics',
		  idAttribute: 'user_topic_id'	
		}, 
		{
		update: Promise.method(function(user_id,obj,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
			UserTopic.query({where: {user_id:user_id}}).fetchAll().then(function(model) {
				var completedata = model.toJSON();
				for (var s in completedata) {
					for (var p in obj) {
					if(obj[p].topic_id == completedata[s].topic_id) 	obj[p] = '';
					}
				}
				var realdata = _.compact(obj);
				//console.log('realdata',realdata);
				new UserTopics(realdata).invokeThen('save', null,null).then(function(response) {
				//console.log('response',response);
						callback(response);
					});  
				});			
		}),
		get: Promise.method(function(user_id,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1
		bookshelf.knex('user_topics').join('topics', 'topics.topic_id', '=', 'user_topics.topic_id').where('user_topics.user_id',user_id).select().
			then(function(response) 
			{
				callback(response);
			});		
		}),
		deleted: Promise.method(function(user_id,obj,callback) {
		
		
		
		
		UserTopic.query({where: obj}).fetchAll().then(function(model) {
				model.invokeThen('destroy').then(function(response) {
					callback(response);
					});
				});	
		//UserTopic.query({where: obj}).fetchAll().then(function(response) {
		//console.log(response.toJSON());
		//		new UserTopics(obj).invokeThen('destroy').then(function(response) {
					//	callback(response);
					//});	
					// UserTopics.query({where:obj}).fetchAll().then(function(model) {
					// model.invokeThen('destroy').then(function() {
						// callback('deleted');				
					// });
				  
				// });	
		})
			
		
		
	});
	var UserTopics = bookshelf.Collection.extend({
			model: UserTopic
		});
module.exports = UserTopic;