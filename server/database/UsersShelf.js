var bcrypt   = Promise.promisifyAll(require('bcrypt'));
var jwt = require('jwt-simple');
var path = require('path');
var secret = 'xxx';
var multiparty = require('connect-multiparty');
var multipartyMiddleware = multiparty();
var fs = require('fs');
var salt = "";
//var emailNotify = require('../../server/notification/emailNotification.js');
var UserSession = require('../../server/database/SessionShelf.js');
bcrypt.genSalt(10, function(err, salt1) {
salt = salt1;
});

	var Users = bookshelf.Model.extend({
	  tableName: 'users',
	  idAttribute: 'user_id',
	  // parse: function(attrs) {
            // return {
				// user_id : attrs.UserId,
				// username: attrs.Username,
				// user_first_name: FirstName,
				// user_last_name : LastName,
                // user_email: attrs.Email,
                // password: attrs.Password,
                // user_gender: attrs.Gender,
                // user_bio: attrs.Bio
            // }
		// },
		initialize: function() {
			this.on('saving', this.validateSave);
		},
		account: function() {
			return this.belongsTo(Account);
		},

	}, {
		authenticate:Promise.method(function(Userid,token,callback) {
			redis.exists("Profile:"+Userid,function(err,status){
			if(err) 
			{
				throw new Error('Redis error in getting ProfileId ');
			}
			else 
			{
				if(debugModeOn) console.log('authenticate : UserShelf : Return request from UserShelf : '+ status);
				if(status)
				{
					
				
					redis.hget("Profile:"+Userid,token,function(erro,value){
					if (erro) 
					{
						throw new Error('Redis error in authentication ');
					}
					else
					{
						//console.log("hash field value",value);
						if(value)
						{
						
							redis.set("Profile1:"+Userid,10,"EX",3600);
								if(debugModeOn) console.log('Authentication : UserShelf : Token is successfully authenticated : ',value);
							callback(true);
						}
						else
						{
							if(debugModeOn) console.log('value not exist');
							callback(false);
						}
					}
					});
				}
				else
				{
					
					UserSession.getSession(Userid,token).then(function(session){
					if(session)
					{
						//console.log('token db valid');
						if(session.attributes.logout_timestamp)
						{
							console.log("session.attributes.Logout",session.attributes.Logout);
							callback(false); //redirect to login page
						}
						else
						{
							console.log("!session.attributes.UserId",session.attributes.UserId);
							Users.forge({user_id:session.attributes.user_id}).fetch().then(function(model1) {
								if (model1) { 
									console.log('user model found');
											redis.hmset("Profile:"+model1.attributes.user_id,"Email",model1.attributes.user_email,token,"token",function(err,val){
											console.log("redis keys set - ready to use website without new login"+ err +"   "+ val);
											callback(true);
											})
								} else throw new Error("User not found");
							});
						}
						
					}
					else
					{
						//console.log('Found user +  NO session found in sessions ');
						callback(false);
					}
					});
				}
			}
			
			});
		
		
		}),
		
		login: Promise.method(function(Email, Password) {
		console.log("email on login ",Email, Password)
		return new this({user_email: Email.toLowerCase().trim()}).fetch({require: true}).tap(function(user) {
		var hash = bcrypt.hashSync(Password,user.get('user_salt'));
		if(user.get('password') == hash) return true;
		else throw new Error('Email or Password is incorrect');
		
		});

		
	  }),
	  changePassword: Promise.method(function(Userid,oldPassword,newPassword,callback) {
		
		Users.forge({user_id:Userid}).fetch().then(function(model) {
						if (model) 
						{
							
							var hash = bcrypt.hashSync(oldPassword,model.get('user_salt'));
							console.log('bcrypt',hash);
							if(model.get('password') == hash)
							{
								model.set({password:bcrypt.hashSync(newPassword,model.get('user_salt'))}).save().then(function(model1) {
									if(model1)
									{
										console.log('password change first then data',model1);
										callback(model1);
									}
									else
									{
										console.log('false with');
										callback(false);
									}
								});
							}
							else 
							{
								console.log('false with hash');
								callback(false);
							}
						}	
					});
	  }),
	  checkPassword: Promise.method(function(Userid,oldPassword) {
		return Users.forge({user_id:Userid}).fetch().then(function(model) {
						if (model) 
						{
							var hash = bcrypt.hashSync(oldPassword,model.get('user_salt'));
							if(model.get('password') == hash)
							{
								return model;
							}
							else 
							{
								return false;
							}
						}
						
					});
	  }),
	  
	  
	  
	  	register: Promise.method(function(obj) {
		if(obj)
		{
			try
			{
				console.log('usershelf:register',obj,salt)
				var hash = bcrypt.hashSync(obj.password, salt);
				obj.user_salt = salt
				obj.password = hash;
					return new Users(obj).save().then(function(model) {
					console.log('model',model)
					return model;
				});
			}
			catch(err) {
				console.log("Error in UserShelf func Register",err)
				return false;
			}
		}
		else 
		{
			console.log('usershelf:register:obj is undefined')
			return false;
		}
	  }),
		getUser: Promise.method(function(id) {
			return new Users({'user_id': id}).fetch().then(function(users) {
			if(users) return users;
			else return;
		})
			
		}),
		twitterUser: Promise.method(function(obj) {
			
			var hash = bcrypt.hashSync(obj.password, salt);
			obj.user_salt = salt
			obj.password = hash;
				return new Users(obj).save().then(function(model) {
			return model;
		});
			
		}),
		check: Promise.method(function(obj) {
		return new this(obj).fetch({require: true}).tap(function(user) {
			if(user) return user;
			else return false;
		});
	  }),
	  
	  
	  checking: Promise.method(function(id) {
		if(id)
		{
			//console.log('id in user shelf',id)
			return Users.forge({user_email:id}).fetch().then(function(user) {
			//console.log('user id in user shelf',user.attributes.user_id)
			if(user) return user;
			else return false;
			
			});
			}
			else{
				console.log('socialshelf:checking for user in user table : id is not defined')
				return false;
			}
	  }),
		update: Promise.method(function(obj) {
			return Users.forge({user_id:obj.user_id}).fetch().then(function(model) {
			if (model) { 
				model.set(obj).save().then(function(model) {
					return model;
				}).otherwise(function(err) {
								throw new Error(err.message);
							});
						} else throw new Error("User not found");
					});
		}),
		logout: Promise.method(function(obj) {
			
		}),
		friends: Promise.method(function(user_id,callback) { //{where: {other_id: '5'}, orWhere: {key: 'value'} 'where', 'Active', '=', 1 andWhere('user_connections.user_id', '!=', user_id)
				//new Users().fetchAll().then(function(allcon) {
		bookshelf.knex('users').join('user_connections', 'user_connections.connection_user_id', '!=', 'users.user_id')
		.where('user_connections.user_id','=',user_id).groupBy('users.user_id').select('users.user_id','users.username','user_avatar')
		.then(function(allcon) {
					callback(allcon);
			})
		})
	
});
	module.exports = Users;
	