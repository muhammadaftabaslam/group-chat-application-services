var mysql = require('mysql');
var dbConfigurations = require('../../config/database.js');
var connection = mysql.createConnection(dbConfigurations);
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);
});
module.exports = 
{
	//must send complete Json object in parameter
	addUser: function(obj,callback)
	{
		console.log("addUser");
		module.exports.checkUserField({Email : obj.Email},function(err,status){
		if(!status)
		{
			var queryString = 'INSERT INTO Users SET ?';
			connection.query(queryString, obj, function(err, rows, fields) {
				if (err)
				{			
					console.log("err",err);
					callback(err,null);
				}
				else 
				{
					console.log("rows",rows.insertId);
					callback(null,rows.insertId);
				}
				});
		}
		else
		{
			console.log("alreadyexistindb");
			callback("alreadyexistindb",null);
		}
		});
		
	},
	//Send json object field value and it will return if it exist or not
	// Return false if a single field not match

	checkUserField: function(myObj,callback)
	{
		console.log("checkUserField");
		var val = [];
		var queryString = "SELECT * FROM Users WHERE";
		var incre = 0;
		for (k in myObj)
		{
			if (myObj.hasOwnProperty(k))
			{
				if(incre > 0)
				{
					queryString = queryString + " && ";	
				}
				val.push(k);
				val.push(myObj[k]);
				queryString = queryString + "?? = ?";
				++incre;
			}
		}
		var a = connection.query(queryString+";",val, function(err, rows, fields) {
		console.log("sql : ",a.sql);
		if(err)
		{
			console.log("err",err);
			callback(err,null);
		}
		else 
		{
			if(rows.length < 1)
			{
				console.log(false);
				callback(null,false);
			}
			else
			{
				console.log(true);
				callback(null,true);
			}
		}
		});
	
	}



}