module.exports = function(app) {
		
	require('../config/auth.js');
	require('./database/redisSession.js');
	require('./controllers/UserCtrl.js')(app);
	require('./controllers/UserTopicCtrl.js')(app);
	require('./controllers/SearchCtrl.js')(app);
	require('./controllers/NotificationsCtrl.js')(app);
	require('./controllers/TopicCtrl.js')(app);
	require('./API/index.js')(app)
	require('./Socket/SocketCtrl.js');
	require('./controllers/ConnectionCtrl.js')(app);
	require('./controllers/ConversationRoomCtrl.js')(app);
	require('./controllers/SocialCtrl.js')(app);
	require('./controllers/FeaturesCtrl.js')(app);
	require('./controllers/ConversationRoomMemberCtrl.js')(app)
	//require('./database/UserTopicShelf.js')(app);
	//require('./controllers/rooms.js')(app);
	};
	