exports.CreateRoom = function(data) {
	//console.log('redis CreateRoom',data);
	//redis.hset("Profile:"+data.user_id,"room_id",data.conversation_room_id);
	//redis.rpush('Room:Admins:' + data.conversation_room_id,JSON.stringify(data),function(erradmins,adminslist){
	
		//console.log('adminslist',erradmins,adminslist);
		//data.index = adminslist;
		//redis.hset('rooms:' + data.conversation_room_id + ':online', data.user_id,JSON.stringify(data));
	//	redis.zadd('Room:Admins:ID:' + data.conversation_room_id, 1,data.user_id,function(err, status) {});
	//});
	redis.hmset('rooms:' + data.conversation_room_id + ':info', data, function(err, ok) {
		if(!err && ok) {
		  //redis.hset('convo:rooms:keys',data.conversation_room_topic+':'+encodeURIComponent(data.conversation_room_name), data);
		  redis.zadd('Active_Topics',1,data.topic_id,function(err, status) {});
		 // redis.sadd('convo:public:rooms', data);
		   //fn(null,{Roomkey:roomKey});
		} else {
		   //fn("Some Error on saving Room in Redis",false);
		}
	});
};
exports.updateRoom = function(data) {
	
	redis.hmset('rooms:' + data.conversation_room_id + ':info', data, function(err, ok) {
		if(!err && ok) {
		  //redis.hset('convo:rooms:keys',data.conversation_room_topic+':'+encodeURIComponent(data.conversation_room_name), data);
		 // redis.sadd('convo:public:rooms', data); 
		} else {
		}
	});
};

exports.joinRoomInRedis = function(data,fn) {
var response = {};
response.Messages = [];
redis.hset("Profile:"+data.user_id,"room_id",data.conversation_room_id);
response.conversation_room_id = data.conversation_room_id;
	redis.rpush('Room:Admins:' + data.conversation_room_id,JSON.stringify(data),function(erradmins,adminslist){
		redis.zadd('Room:Admins:ID:' + data.conversation_room_id, 1,data.user_id);
						//redis.hset('rooms:' + data.conversation_room_id + ':online', data.user_id,JSON.stringify(data));
						// save index of admin list in the room online users hash for later usage
			});
	redis.zadd("Online",2,data.user_id,function(errofzadd,dataofzadd){
		redis.hincrby('rooms:' + data.conversation_room_id + ':info', 'MaxUsers', 1);
		//redis.hget('rooms:' + data.conversation_room_id + ':info', 'Messages',function(err,NoOfMsgs){
			redis.lrange('Room:Messages:' + data.conversation_room_id,-10,-1,function(error,lastmessages){
				redis.llen('Room:Messages:' + data.conversation_room_id,function(error,len){
					redis.lrange('Room:Admins:' + data.conversation_room_id,0,1,function(error,admins){
					exports.parseArray(lastmessages,function(messages){
					exports.parseArray(admins,function(parsedAdmins){
					response.Admins = parsedAdmins;
					response.Messages = messages;//Allmessages;
					response.TotalMessages = len;
						//redis.hincrby('rooms:' + data.conversation_room_id + ':info', 'OnlineUsers', 1,function(err,noofusers){
						response.TotalOnlineUsers = parsedAdmins.length;
						console.log('joinRoomInRedis',response);
							fn(response);
						//	});
							})
							});
						});
					});
				});
		//	});
		});
	
};

exports.getRoomData = function(data,fn) {
var response = {};
response.Messages = [];

response.conversation_room_id = data.conversation_room_id;
	redis.zadd("Online",2,data.user_id,function(errofzadd,dataofzadd){
		
		//redis.hget('rooms:' + data.conversation_room_id + ':info', 'Messages',function(err,NoOfMsgs){
			redis.lrange('Room:Messages:' + data.conversation_room_id,-10,-1,function(error,lastmessages){
				redis.llen('Room:Messages:' + data.conversation_room_id,function(error,len){
					redis.lrange('Room:Admins:' + data.conversation_room_id,0,1,function(error,admins){
					exports.parseArray(lastmessages,function(messages){
					exports.parseArray(admins,function(parsedAdmins){
					response.Admins = parsedAdmins;
					response.Messages = messages;//Allmessages;
					response.TotalMessages = len;
						response.TotalOnlineUsers = parsedAdmins.length;
						console.log('joinRoomInRedis',response);
							fn(response);
						
							})
							});
						});
					});
				});
		//	});
		});
	
};

exports.parseArray = function(obj,fn) {
var res = [];
obj.forEach(function(key) {
    res.push(JSON.parse(key));
});
	fn(res);
};


exports.ifAdmins = function(conversation_room_id,user_id,fn) {
	redis.zrank('Room:Admins:ID:' + conversation_room_id, user_id, function(err, ok) {
		if(ok && (ok == 0 || ok == 1)) fn(true);
		else fn(false);
	});
};


exports.closeRoom = function(data,closedroom) {
	redis.del('rooms:' + data.conversation_room_id + ':info');
	redis.zadd('Active_Topics',-1,closedroom.topic_id,function(err, status) {});
	redis.del('Room:Admins:ID:' + data.conversation_room_id);
	redis.del('Room:Admins:' + data.conversation_room_id);
	//redis.hset('convo:rooms:keys',data.conversation_room_topic+':'+encodeURIComponent(data.conversation_room_name), data);
	//redis.sadd('convo:public:rooms', data);
	// Time to save messages or do whatever you want
	//redis.del('Room:Messages:'+data.conversation_room_id);
};





